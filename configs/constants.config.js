import {Dimensions} from 'react-native';

export const AppInfo = {
  name: 'Nimawat Public School',
  version: '1.0.0',
};

export const COLORS = {
  primary: '#002e5d',
  white: '#ffffff',
};

export const SCREEN = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

export const API_BASE = 'https://www.schoolerp.in/nimawat/WebServices/';
