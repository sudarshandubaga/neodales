import React, { Component } from "react";
import { createDrawerNavigator } from '@react-navigation/drawer';
import HomeScreen from "../Screens/Home/HomeScreen";
import DrawerContent from "./DrawerContent";
import { AppInfo, COLORS } from "../configs/constants.config";

const Drawer = createDrawerNavigator();

export default class DrawerNavigation extends Component {
  render() {
    return (
      <Drawer.Navigator
        drawerContent={() => <DrawerContent {...this.props} />}
        screenOptions={{
          headerStyle: {
            backgroundColor: COLORS.primary,
          },
          headerTintColor: COLORS.white
        }}
      >
        <Drawer.Screen
          name="Home"
          component={HomeScreen}
          options={{ title: AppInfo.name }}
        />
      </Drawer.Navigator>
    );
  }
}