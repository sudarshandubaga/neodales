import AsyncStorage from "@react-native-async-storage/async-storage";
import { DrawerContentScrollView } from "@react-navigation/drawer";
import React, { Component } from "react";
import { Image, Linking, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { AppInfo, COLORS } from "../configs/constants.config";
import { styles } from "../Screens/AmazingFriday/style";

export default class DrawerContent extends Component {
    state = {
        user: null,
        integration: {}
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }

        let integration = await AsyncStorage.getItem('@integration');
        this.setState({ integration: JSON.parse(integration) });
    }
    componentDidUpdate = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }
    logOut = async () => {
        await AsyncStorage.setItem('@user', '');
        this.goTo('Login');
    }
    goTo = path => {
        let { navigation } = this.props;
        navigation.navigate(path);
    }
    render() {
        let user = this.state.user;
        return (
            <>
                <View style={{ backgroundColor: COLORS.primary, padding: 10 }}>
                    <Image
                        source={{ uri: this.state.user ? user.Data.photo : '' }}
                        resizeMode="contain"
                        style={{ height: 100, borderRadius: 100 }}
                    />
                    <Text style={{ fontSize: 18, textAlign: 'center', color: '#fff', padding: 10 }}>{this.state.user ? user.Data.name : ''}</Text>
                    {
                        this.state.user && user.UserType == 'S' ?
                            <>
                                <Text style={{ textAlign: 'center', color: '#fff', padding: 5 }}>Class : {this.state.user ? user.Data.class : ''}</Text>
                                <Text style={{ textAlign: 'center', color: '#fff', }}>Enrollment Number : {this.state.user ? user.Data.enrollment_number : ''}</Text>
                            </>
                            :
                            <>
                                <Text style={{ textAlign: 'center', color: '#fff', padding: 5 }}>Profile : {this.state.user ? user.Data.profile : ''}</Text>
                                <Text style={{ textAlign: 'center', color: '#fff', }}>Employee Type : {this.state.user ? user.Data.employee_type : ''}</Text>
                            </>
                    }
                </View>
                <DrawerContentScrollView {...this.props}>
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('Download')}>
                        <Text style={style.navList}>Downloads</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('Contact')}>
                        <Text style={style.navList}>Contact Us</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => Linking.openURL(this.state.integration.fb_url)}>
                        <Text style={style.navList}>Like Us</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => this.navigateTo('ChangePassword')}>
                        <Text style={style.navList}>Change Password</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={style.list} onPress={() => this.logOut()}>
                        <Text style={style.navList}>LogOut</Text>
                    </TouchableOpacity>
                </DrawerContentScrollView>
            </>
        )

    }
}

const style = StyleSheet.create({
    navList: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        fontSize: 16,
        color: COLORS.primary
    },
    list: {
        borderBottomWidth: 1,
        borderColor: COLORS.primary,
    }
});