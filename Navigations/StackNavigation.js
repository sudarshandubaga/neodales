import React, { Component } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Image, Text, View } from "react-native";

import LoginScreen from "../Screens/Login/LoginScreen";
import SendOtpScreen from "../Screens/SendOtp/SendOtpScreen";
import VerifyOtpScreen from "../Screens/VerifyOtp/VerifyOtpScreen";
// import HomeScreen from "../Screens/Home/HomeScreen";
import DrawerNavigation from "./DrawerNavigation";
import MenuScreen from "../Screens/Menu/MenuScreen";
import { AppInfo, COLORS } from "../configs/constants.config";
import AmazingFridayScreen from "../Screens/AmazingFriday/AmazingFridayScreen";
import HolidayScreen from "../Screens/Holiday/HolidayScreen";
import EventScreen from "../Screens/Event/EventScreen";
import EventDetailsScreen from "../Screens/Event/EventDetailsScreen";
import MomentScreen from "../Screens/Moment/MomentScreen";
import WhatsNewScreen from "../Screens/WhatsNew/WhatsNewScreen";
import DownloadScreen from "../Screens/Download/DownloadScreen";
import ContactScreen from "../Screens/Contact/ContactScreen";
import AsyncStorage from "@react-native-async-storage/async-storage";
import PersonalScreen from "../Screens/PersonalInfo/PersonalScreen";
import AttendanceScreen from "../Screens/Attendance/AttendanceScreen";
import FeeScreen from "../Screens/Fees/FeeScreen";
import ImprestScreen from "../Screens/Imprest/ImprestScreen";
import TransportScreen from "../Screens/Transport/TransportScreen";
import EdiaryScreen from "../Screens/Ediary/EdiaryScreen";
import CalenderScreen from "../Screens/Calender/CalenderScreen";
import LeaveScreen from "../Screens/Leave/LeaveScreen";
import VideoScreen from "../Screens/Video/VideoScreen";
import AssignmentScreen from "../Screens/Assignment/AssignmentScreen";
import AssignmentDetailsScreen from "../Screens/Assignment/AssignmentDetailsScreen";
import QuestionScreen from "../Screens/Question/QuestionScreen";

import AttendanceStaffScreen from "../Screens/Stafff/Attendance/AttendanceScreen";
import LeaveStatusScreen from "../Screens/Stafff/LeaveStatus/LeaveStatusScreen";
import SalaryScreen from "../Screens/Stafff/Salary/SalaryScreen";
import ServiceScreen from "../Screens/Stafff/Service/ServiceScreen";
import ComplantScreen from "../Screens/Stafff/Complant/ConplantScreen";
import ChangePasswordScreen from "../Screens/ChangePassword/ChangePasswordScreen";
import TimetableScreen from "../Screens/Timetable/TimetableScreen";
import LoanScreen from "../Screens/Loan/LoanScreen";
import LoanDetailsScreen from "../Screens/Loan/LoanDetailsScreen";
import VideoLectureScreen from "../Screens/VideoLecture/VideoLectureScreen";
import { general_settings } from "../Api/menu";
import SiblingsScreen from "../Screens/Siblings/SiblingsScreen";
import ClassTestScreen from "../Screens/ClassTest/ClassTestScreen";

const Stack = createNativeStackNavigator();

export default class StackNavigation extends Component {
  state = {
    user: null,
    loading: true,
  };


  componentDidUpdate = async () => {
    let user = await AsyncStorage.getItem('@user');
    if (user) {
      user = JSON.parse(user);
      this.setState({ user: user });
    }
  }

  async componentDidMount() {
    // await AsyncStorage.setItem('@user', null);
    let user = await AsyncStorage.getItem('@user');
    if (user) {
      user = JSON.parse(user);
      this.setState({
        user: user,
      });
    }

    let response = await general_settings();

    await AsyncStorage.setItem('@integration', JSON.stringify(response.data.response.integrations));

    setTimeout(() => {
      this.setState({ loading: false })
    }, 5000);
  }

  render() {
    let self = this;
    if (this.state.loading == false) {
      if (this.state.user == null) {
        return (
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{
                headerTintColor: COLORS.white,
                headerStyle: {
                  backgroundColor: COLORS.primary
                }
              }}
            >
              <Stack.Screen
                name="Login"
                options={{
                  headerShown: false
                }}
              >
                {(props) => <LoginScreen onLoginSuccess={user => {
                  self.setState({ user });
                  setTimeout(() => {
                    props.navigation.push('Home');
                  }, 100);
                }} />}
              </Stack.Screen>
              <Stack.Screen
                name="Home"
                component={DrawerNavigation}
                options={{
                  headerShown: false
                }}
              />
              <Stack.Screen
                name="SendOtp"
                component={SendOtpScreen}
                options={{
                  headerShown: true,
                  title: 'Send Otp'
                }}
              />
              <Stack.Screen
                name="VerifyOtp"
                component={VerifyOtpScreen}
                options={{
                  headerShown: true,
                  title: 'Verify Otp'
                }}
              />
            </Stack.Navigator>
          </NavigationContainer>
        );
      } else {
        return (
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{
                headerTintColor: COLORS.white,
                headerStyle: {
                  backgroundColor: COLORS.primary
                }
              }}
            >
              <Stack.Screen
                name="Home"
                component={DrawerNavigation}
                options={{
                  headerShown: false
                }}
              />
              <Stack.Screen
                name="Menu"
                component={MenuScreen}
                options={{
                  title: 'Menu Of The Month'
                }}
              />
              <Stack.Screen
                name="AmazingFriday"
                component={AmazingFridayScreen}
                options={{
                  title: 'Amazing Friday'
                }}
              />
              <Stack.Screen
                name="Holiday"
                component={HolidayScreen}
                options={{
                  title: 'Holiday Of The Month'
                }}
              />
              <Stack.Screen
                name="Event"
                component={EventScreen}
                options={{
                  title: 'Forthcoming Events'
                }}
              />
              <Stack.Screen
                name="EventDetails"
                component={EventDetailsScreen}
                options={{
                  title: 'Event Details'
                }}
              />
              <Stack.Screen
                name="Gallery"
                component={MomentScreen}
                options={{
                  title: 'Mesmerising Moments'
                }}
              />
              <Stack.Screen
                name="News"
                component={WhatsNewScreen}
                options={{
                  title: 'What\'s New'
                }}
              />
              <Stack.Screen
                name="Download"
                component={DownloadScreen}
                options={{
                  title: 'Downloads'
                }}
              />
              <Stack.Screen
                name="Contact"
                component={ContactScreen}
                options={{
                  title: 'Contact Us'
                }}
              />
              <Stack.Screen
                name="Profile"
                component={PersonalScreen}
                options={{
                  title: 'Personal Info'
                }}
              />
              <Stack.Screen
                name="Siblings"
                options={{
                  title: 'Sibling Info'
                }}
              >
                {(props) => <SiblingsScreen onLoginSuccess={user => {
                  // console.log('logged in user: ', user);
                  self.setState({ user });
                  setTimeout(() => {
                    props.navigation.push('Home');
                  }, 100);
                }} />}
              </Stack.Screen>
              <Stack.Screen
                name="Attendance"
                component={AttendanceScreen}
                options={{
                  title: 'Attendance Info'
                }}
              />
              <Stack.Screen
                name="ClassTest"
                component={ClassTestScreen}
                options={{
                  title: 'Class Test'
                }}
              />
              <Stack.Screen
                name="Fees"
                component={FeeScreen}
                options={{
                  title: 'Fees Info'
                }}
              />
              <Stack.Screen
                name="Imprest"
                component={ImprestScreen}
                options={{
                  title: 'Imprest Info'
                }}
              />
              <Stack.Screen
                name="Transport"
                component={TransportScreen}
                options={{
                  title: 'Transport Info'
                }}
              />
              <Stack.Screen
                name="Ediary"
                component={EdiaryScreen}
                options={{
                  title: 'E-Diary'
                }}
              />
              <Stack.Screen
                name="Calender"
                component={CalenderScreen}
                options={{
                  title: 'Calender'
                }}
              />
              <Stack.Screen
                name="Leave"
                component={LeaveScreen}
                options={{
                  title: 'Apply Leave'
                }}
              />
              <Stack.Screen
                name="Question"
                component={QuestionScreen}
                options={{
                  title: 'Ask Question'
                }}
              />
              <Stack.Screen
                name="Videos"
                component={VideoScreen}
                options={{
                  title: 'Video Gallery'
                }}
              />
              <Stack.Screen
                name="VideosLectures"
                component={VideoLectureScreen}
                options={{
                  title: 'Video Lectures'
                }}
              />
              <Stack.Screen
                name="Assignments"
                component={AssignmentScreen}
                options={{
                  title: 'Assignments'
                }}
              />
              <Stack.Screen
                name="AssignmentDetails"
                component={AssignmentDetailsScreen}
                options={{
                  title: 'Assignment Details'
                }}
              />
              <Stack.Screen
                name="AttendanceStaff"
                component={AttendanceStaffScreen}
                options={{
                  title: 'Attendance Info'
                }}
              />
              <Stack.Screen
                name="LeaveStatus"
                component={LeaveStatusScreen}
                options={{
                  title: 'Leave Status'
                }}
              />
              <Stack.Screen
                name="Salary"
                component={SalaryScreen}
                options={{
                  title: 'Salary Details'
                }}
              />
              <Stack.Screen
                name="Service"
                component={ServiceScreen}
                options={{
                  title: 'Service Record'
                }}
              />
              <Stack.Screen
                name="Complant"
                component={ComplantScreen}
                options={{
                  title: 'Complant Status'
                }}
              />
              <Stack.Screen
                name="ChangePassword"
                component={ChangePasswordScreen}
                options={{
                  title: 'Change Password'
                }}
              />
              <Stack.Screen
                name="Timetable"
                component={TimetableScreen}
                options={{
                  title: 'Time Table'
                }}
              />
              <Stack.Screen
                name="Loan"
                component={LoanScreen}
                options={{
                  title: 'Loan Details'
                }}
              />
              <Stack.Screen
                name="LoanDetails"
                component={LoanDetailsScreen}
                options={{
                  title: 'Loan Details'
                }}
              />
              <Stack.Screen
                name="Login"
                options={{
                  headerShown: false
                }}
              >
                {(props) => <LoginScreen onLoginSuccess={user => {
                  // console.log('logged in user: ', user);
                  self.setState({ user });
                  setTimeout(() => {
                    props.navigation.push('Home');
                  }, 100);
                }} />}
              </Stack.Screen>

            </Stack.Navigator>
          </NavigationContainer>
        );
      }
    } else {
      return (
        <View style={{ backgroundColor: COLORS.primary, position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, justifyContent: 'center', alignItems: 'center' }}>
          {/* <ActivityIndicator size="large" color={COLORS.white} /> */}

          <Image
            source={require('../Assets/logo-white.png')}
            resizeMode="contain"
            style={{ height: 300 }}
          />
          <Text style={{ color: COLORS.white, fontSize: 18 }}>{AppInfo.name}</Text>
        </View>
      );
    }
  }
}