// import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { API_BASE } from "../configs/constants.config";

// export default ApiExecute = async (url, params = {}) => {
//     let api_params = {
//         cache: 'no-cache',
//         mode: 'cors'
//     };
//     api_params.method = params.method ?? 'GET';
//     api_params.headers = {
//         Accept: 'application/json',
//         'Content-Type': 'application/json'
//     }
//     if (params.data) {
//         api_params.body = JSON.stringify(params.data);
//     }
//     if (params.auth) {
//         let token = await AsyncStorage.getItem('@token');
//         api_params.headers["x-access-token"] = token;
//     }

//     console.log('api url: ', `${API_BASE}${url}`);
    
//     return fetch(`${API_BASE}${url}`, api_params)
//         .then(response => response.json())
//         .then(json => {
//             return { status: true, data: json };
//         })
//         .catch(err => {
//             return { status: false, error: err };
//         })
// }

const ApiExecute = async (url, params = {}) => {
    console.log(API_BASE + url);
    let headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    let instance = axios.create({
        baseURL: API_BASE,
        timeout: 1000,
        headers
    });

    let method = params.method ?? 'GET';
    let data = params.data ?? null;

    return instance.request({
        url,
        method,
        data
    })
        .then(res => {
            console.log('response: ', res);
            return {
                status: true,
                data: res.data
            }
        })
        .catch(err => {
            console.log('API Error: ', url, err);
            return {
                status: false,
                error: err
            }
        });
}
export default ApiExecute