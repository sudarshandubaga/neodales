import ApiExecute from "./";

export const general_settings = async () => {
    let response = await ApiExecute('integration');
    return response;
}

export const view_menus = async () => {
    let response = await ApiExecute('menu_of_month');
    console.log('response: ', response);
    return response;
}
export const view_color_fridays = async () => {
    let response = await ApiExecute('color_fridays');

    return response;
}

export const view_holidays = async () => {
    let response = await ApiExecute('holiday_of_the_month');

    return response;
}
export const view_events = async () => {
    let response = await ApiExecute('events');

    return response;
}
export const view_gallery = async () => {
    let response = await ApiExecute('gallery');

    return response;
}
export const view_videos = async () => {
    let response = await ApiExecute('video');

    return response;
}
export const view_whatsnew = async () => {
    let response = await ApiExecute('news');

    return response;
}
export const view_downloads = async () => {
    let response = await ApiExecute('downloads');

    return response;
}
