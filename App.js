/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {View} from 'react-native';
import {StatusBar} from 'react-native';
// import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {COLORS} from './configs/constants.config';
import StackNavigation from './Navigations/StackNavigation';

export default class App extends Component {
  render() {
    return (
      <>
        <View backgroundColor={COLORS.primary}>
          <StatusBar
            backgroundColor={COLORS.primary}
            barStyle="light-content"
          />
        </View>
        <StackNavigation {...this.props} />
      </>
    );
  }
}
