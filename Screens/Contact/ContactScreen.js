import React, {Component} from 'react';
import {
  ActivityIndicator,
  Linking,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {COLORS} from '../../configs/constants.config';
import style from './style';
// import { view_menus } from "../../Api/menu";

export default class ContactScreen extends Component {
  state = {
    menus: null,
    loading: true,
  };
  componentDidMount = async () => {
    // let response = await view_menus();

    // if (response.status) {
    //     this.setState({ menus: response.data.response ?? null, loading: false });
    // }
    this.setState({loading: false});
  };
  render() {
    // if (this.state.menus)
    //     console.log('response: ', this.state.menus.menu_list[0].menu);
    return (
      <ScrollView>
        <View style={style.container}>
          {this.state.loading ? (
            <View style={{padding: 10}}>
              <ActivityIndicator size="large" color={COLORS.primary} />
            </View>
          ) : (
            <View>
              <View style={{marginBottom: 10}}>
                <Text style={style.Heading}>Contact Number</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 10,
                    paddingBottom: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() => Linking.openURL('tel:8003693000')}>
                    <Text>+91 8003693000 </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => Linking.openURL('tel:8003692991')}>
                    <Text>/ +91 8003692991</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{marginBottom: 10}}>
                <Text style={style.Heading}>E-mail</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 10,
                    paddingBottom: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() =>
                      Linking.openURL('mailto:nimawat98@gmail.com')
                    }>
                    <Text>nimawat98@gmail.com</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{marginBottom: 10}}>
                <Text style={style.Heading}>Website</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 10,
                    paddingBottom: 10,
                  }}>
                  <TouchableOpacity
                    onPress={() =>
                      Linking.openURL('https://www.nimawatpublicschool.org/')
                    }>
                    <Text>www.nimawatpublicschool.org</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{marginBottom: 10}}>
                <Text style={style.Heading}>Address</Text>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingTop: 10,
                    paddingBottom: 10,
                  }}>
                  <TouchableOpacity>
                    <Text>
                      Nimawat Public School, Fatehpur-Shekhawati - 332301 Dist -
                      Sikar, Rajasthan (INDIA)
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    );
  }
}
