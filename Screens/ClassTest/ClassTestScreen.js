import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, ScrollView, StyleSheet, Text, View, Picker } from "react-native";
import ApiExecute from "../../Api";
import { attendance } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "./style";

export default class ClassTestScreen extends Component {
    state = {
        subject: null,
        classTests: [],
        subjects: [],
        loading: true
    }
    fetchClassTest = async subject => {
        this.setState({ subject });

        let user = this.state.user;

        let url = `classTest/${user.Data.student_id}/${user.Data.section_id}/${subject}`;

        let response = await ApiExecute(url);

        console.log('class tests:', url, response.data);


        if (response.status) {
            this.setState({ classTests: response.data.response.data ?? null, loading: false });
        }

    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });

            let api_response = await ApiExecute(`classTestSubjects/${user.Data.student_id}/${user.Data.section_id}`);


            if (api_response.status) {
                this.setState({ subjects: api_response.data.response.data, loading: false });
            }



        }
    }
    render() {
        // if (this.state.classTests)
        // console.log('response: ', this.state.classTests);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <>
                                <View style={{ borderWidth: 2, borderColor: COLORS.primary, marginBottom: 15, borderRadius: 8 }}>
                                    <Picker
                                        selectedValue={this.state.subject}
                                        style={{ height: 50, padding: 0, borderWidth: 1, borderColor: COLORS.primary }}
                                        onValueChange={subject => this.fetchClassTest(subject)}
                                    >
                                        <Picker.Item label="Select Subject" value={null} />
                                        {
                                            this.state.subjects && Object.entries(this.state.subjects).map((row, i) => {
                                                return (
                                                    <Picker.Item label={row[1]} value={row[0]} />
                                                )
                                            })
                                        }
                                    </Picker>
                                </View>
                                <View>
                                    {
                                        Object.values(this.state.classTests).length ?
                                            <>
                                                <View style={style.Row}>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, fontWeight: 'bold' }}>Date</Text>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, fontWeight: 'bold' }}>Marks</Text>
                                                </View>
                                                {
                                                    this.state.classTests && Object.values(this.state.classTests).map((row, i) => {
                                                        return (
                                                            <View style={style.Row}>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1 }}>{row.date}</Text>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1 }}>{row.marks}</Text>
                                                            </View>
                                                        )
                                                    })
                                                }
                                            </>
                                            : <></>
                                    }
                                </View>
                            </>

                    }
                </View>
            </ScrollView >
        );
    }
}