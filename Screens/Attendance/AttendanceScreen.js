import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, ScrollView, StyleSheet, Text, View } from "react-native";
import { attendance } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "./style";

export default class AttendanceScreen extends Component {
    state = {
        attendance: null,
        loading: true
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `attendance/${user.Data.student_id}/${user.Data.section_id}`;

            let response = await attendance(url);

            if (response.status) {
                this.setState({ attendance: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.attendance)
            console.log('response: ', this.state.attendance);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <>
                                <View>
                                    {
                                        this.state.attendance && this.state.attendance.header ?
                                            <View style={style.Row}>
                                                <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{this.state.attendance.header[0]}</Text>
                                                <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{this.state.attendance.header[1]}</Text>
                                                <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1 }}>{this.state.attendance.header[2]}</Text>
                                                <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1 }}>{this.state.attendance.header[3]}</Text>
                                            </View>
                                            : ''
                                    }
                                </View>
                                <View>
                                    {
                                        this.state.attendance && this.state.attendance.data && this.state.attendance.data.map((row, i) => {
                                            return (
                                                <View style={style.Row}>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{row.Month}</Text>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{row.Total}</Text>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1 }}>{row.P}</Text>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>{row.A}</Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </>

                    }
                </View>
            </ScrollView >
        );
    }
}