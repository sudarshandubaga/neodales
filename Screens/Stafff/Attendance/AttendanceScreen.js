import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, ScrollView, StyleSheet, Text, Table, Row, Rows, View } from "react-native";
import { attendance } from "../../../Api/student";
import { COLORS } from "../../../configs/constants.config";
import style from "./style";

export default class AttendanceScreen extends Component {
    state = {
        attendance: null,
        loading: true
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `employee_attendance/${user.Data.employee_id}`;

            let response = await attendance(url);

            if (response.status) {
                this.setState({ attendance: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.attendance)
            console.log('response: ', this.state.attendance);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <>
                                <ScrollView horizontal>
                                    <View>
                                        {
                                            this.state.attendance && this.state.attendance.header ?
                                                <View style={style.Row}>
                                                    <Text style={{ flex: 9, width: 100, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>Month</Text>
                                                    <Text style={{ flex: 7, width: 70, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>Total</Text>
                                                    <Text style={{ flex: 3, width: 40, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>P</Text>
                                                    <Text style={{ flex: 3, width: 40, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>A</Text>
                                                    <Text style={{ flex: 3, width: 40, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>CL</Text>
                                                    <Text style={{ flex: 3, width: 60, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>LWP</Text>
                                                    <Text style={{ flex: 3, width: 50, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>WB</Text>
                                                    <Text style={{ flex: 3, width: 40, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>SB</Text>
                                                    <Text style={{ flex: 3, width: 40, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>EL</Text>
                                                    <Text style={{ flex: 3, width: 40, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>H</Text>
                                                    <Text style={{ flex: 3, width: 60, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>WFH</Text>
                                                    <Text style={{ flex: 3, width: 40, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1 }}>C</Text>
                                                    <Text style={{ flex: 3, width: 40, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>L</Text>
                                                </View>
                                                : ''
                                        }
                                        {
                                            this.state.attendance && this.state.attendance.data && this.state.attendance.data.map((row, i) => {
                                                return (
                                                    <View style={style.Row}>
                                                        <Text style={{ flex: 9, width: 100, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['Month']}</Text>
                                                        <Text style={{ flex: 7, width: 70, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['Total']}</Text>
                                                        <Text style={{ flex: 3, width: 40, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['Present']}</Text>
                                                        <Text style={{ flex: 3, width: 40, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['ABSENT']}</Text>
                                                        <Text style={{ flex: 3, width: 40, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['CASUAL LEAVE']}</Text>
                                                        <Text style={{ flex: 3, width: 60, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['LEAVE WITHOUT PAY']}</Text>
                                                        <Text style={{ flex: 3, width: 50, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['WINTER BREAK']}</Text>
                                                        <Text style={{ flex: 3, width: 40, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['SUMMER BREAK']}</Text>
                                                        <Text style={{ flex: 3, width: 40, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['EARNED LEAVE']}</Text>
                                                        <Text style={{ flex: 3, width: 40, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['HOLIDAY']}</Text>
                                                        <Text style={{ flex: 3, width: 60, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['WORK FROM HOME']}</Text>
                                                        <Text style={{ flex: 3, width: 40, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, }}>{row['CLUBBED']}</Text>
                                                        <Text style={{ flex: 3, width: 40, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderLeftWidth: 0 }}>{row['LOCKDOWN']}</Text>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                </ScrollView>
                            </>

                    }
                </View>
            </ScrollView >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 18,
        paddingTop: 35,
        backgroundColor: '#ffffff'
    },
    HeadStyle: {
        height: 50,
        alignContent: "center",
        backgroundColor: '#ffe0f0'
    },
    TableText: {
        margin: 10
    }
});