import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Button, Linking, ScrollView, StyleSheet, Text, TextInput, View } from "react-native";
import { calender } from "../../../Api/student";
import { COLORS } from "../../../configs/constants.config";
import style from "../Attendance/style";

export default class ServiceScreen extends Component {
    state = {
        service: null,
        loading: true
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `employee_eservice/${user.Data.employee_id}`;
            let response = await calender(url);

            if (response.status) {
                console.log('sajkhdskjf', response.data.response);
                this.setState({ service: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.service)
            console.log('response: ', this.state.service);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                {/* <View>
                                    <TextInput placeholder="All"></TextInput>
                                </View> */}
                                {
                                    this.state.service && this.state.service.data && this.state.service.data.records && this.state.service.data.records.map((row, i) => {
                                        return (
                                            <View style={style.Row}>
                                                <View key={i} style={style.Column}>
                                                    <View>
                                                        <Text>{row.note}</Text>
                                                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                            <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16 }}>{row.sender}</Text>
                                                            <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16, textAlign: 'right' }}>{row.send_date}</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    })
                                }

                                <View style={{ marginTop: 10 }}>
                                    <Button
                                        onPress={() => this.navigateTo('Leave')}
                                        title={`Total Points : ${this.state.service.data.total}`}
                                        color={COLORS.primary}
                                    />
                                </View>
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}