import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Button, Linking, ScrollView, StyleSheet, Text, TextInput, View } from "react-native";
import { calender } from "../../../Api/student";
import { COLORS } from "../../../configs/constants.config";
import style from "../Attendance/style";

export default class SalaryScreen extends Component {
    state = {
        salary: null,
        loading: true
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `employee_salary/${user.Data.employee_id}`;
            let response = await calender(url);

            if (response.status) {
                console.log('sajkhdskjf', response.data.response);
                this.setState({ salary: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.salary)
            console.log('response: ', this.state.salary);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                {/* <View>
                                    <TextInput placeholder="All"></TextInput>
                                </View> */}
                                {
                                    this.state.salary && this.state.salary.data && this.state.salary.data.map((row, i) => {
                                        if (row.id != '') {
                                            return (
                                                <View style={style.Row}>
                                                    <View key={i} style={style.Column}>
                                                        <View>
                                                            <View style={{ flexDirection: 'row', }}>
                                                                <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16 }}>{row.Month}</Text>
                                                                <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16, textAlign: 'right' }}>
                                                                    <Button
                                                                        title="Salary Slip"
                                                                        onPress={() => ('')}
                                                                        color={COLORS.primary}></Button>
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                            )
                                        }
                                    })
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}