import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Button, Linking, ScrollView, StyleSheet, Text, TextInput, View } from "react-native";
import { calender } from "../../../Api/student";
import { COLORS } from "../../../configs/constants.config";
import style from "../Attendance/style";

export default class ComplantScreen extends Component {
    state = {
        complant: null,
        loading: true
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `employee_complaint_status/${user.Data.employee_id}`;
            let response = await calender(url);

            if (response.status) {
                console.log('sajkhdskjf', response.data.response);
                this.setState({ complant: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.complant)
            console.log('response: ', this.state.complant);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                {/* <View>
                                    <TextInput placeholder="All"></TextInput>
                                </View> */}
                                {
                                    this.state.complant && this.state.complant.data && this.state.complant.data.map((row, i) => {
                                        return (
                                            <View style={style.Row}>
                                                <View key={i} style={style.Column}>
                                                    <View>
                                                        <Text style={{ color: COLORS.primary, fontSize: 16 }}>{row.category}</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.area}</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.date}</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.status}</Text>
                                                        <Text style={{ fontSize: 16 }}>{row.name}</Text>
                                                        <Text>{row.description}</Text>
                                                        {/* <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                                            <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16 }}>{row.sender}</Text>
                                                            <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16, textAlign: 'right' }}>{row.send_date}</Text>
                                                        </View> */}
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    })
                                }
                            </View>

                    }
                </View>
            </ScrollView >
        );
    }
}