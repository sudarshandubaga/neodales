import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, ScrollView, StyleSheet, Text, Table, Row, Rows, View, Button, Linking } from "react-native";
import { attendance } from "../../../Api/student";
import { COLORS } from "../../../configs/constants.config";
import style from "../../Attendance/style";

export default class LeaveStatusScreen extends Component {
    state = {
        leave: null,
        loading: true
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `employee_leave_status/${user.Data.employee_id}`;

            let response = await attendance(url);

            if (response.status) {
                this.setState({ leave: response.data.response ?? null, loading: false });
            }
        }
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }
    render() {
        if (this.state.leave)
            console.log('response: ', this.state.leave);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <>
                                <ScrollView horizontal>
                                    <View>
                                        <View style={style.Row}>
                                            <Text style={{ flex: 2.5, width: 120, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>Leave Type</Text>
                                            <Text style={{ flex: 2.5, width: 80, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>Total</Text>
                                            <Text style={{ flex: 2.5, width: 100, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1 }}>Consumed</Text>
                                            <Text style={{ flex: 2.5, width: 100, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>Available</Text>
                                        </View>
                                        {
                                            this.state.leave && this.state.leave.data && this.state.leave.data.map((row, i) => {
                                                return (
                                                    <View style={style.Row}>
                                                        <Text style={{ flex: 2.5, width: 120, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['leave_type']}</Text>
                                                        <Text style={{ flex: 2.5, width: 80, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderRightWidth: 0 }}>{row['total']}</Text>
                                                        <Text style={{ flex: 2.5, width: 100, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, }}>{row['consumed']}</Text>
                                                        <Text style={{ flex: 2.5, width: 100, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderTopWidth: .5, borderLeftWidth: 0 }}>{row['available']}</Text>
                                                    </View>
                                                )
                                            })
                                        }
                                    </View>
                                </ScrollView>

                                <View style={{ marginTop: 10 }}>
                                    <Button
                                        onPress={() => this.navigateTo('Leave')}
                                        title="Apply Leave"
                                        color={COLORS.primary}
                                    />
                                </View>
                            </>

                    }
                </View>
            </ScrollView >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 18,
        paddingTop: 35,
        backgroundColor: '#ffffff'
    },
    HeadStyle: {
        height: 50,
        alignContent: "center",
        backgroundColor: '#ffe0f0'
    },
    TableText: {
        margin: 10
    }
});