import { StyleSheet } from "react-native";
import { COLORS, SCREEN } from "../../../configs/constants.config";

export default StyleSheet.create({
    container: {
        padding: 20,
    },
    Row: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    Column: {
        flex: 1,
        padding: 10,
        borderWidth: 2,
        borderColor: COLORS.primary,
        borderRadius: 5,
        marginBottom: 20,
    },
    Heading: {
        fontSize: 18,
        color: COLORS.primary,
    },
    MainHeading: {
        borderBottomWidth: 2,
        borderColor: COLORS.primary,
        borderColor: COLORS.primary,
        fontSize: 20,
        color: COLORS.primary,
        marginBottom: 20,
    }
});