import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Picker, ScrollView, StyleSheet, Text, TextInput, View } from "react-native";
import { calender } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Menu/style";

export default class TimetableScreen extends Component {
    state = {
        timetable: null,
        loading: true,
        day: '',
        days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');

        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var day = new Date();
        var dayName = days[day.getDay()];

        this.setState({ day: dayName });
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url;
            if (user.UserType == 'S') {
                url = `timetable/${user.Data.student_id}/${user.Data.section_id}`;
            } else {
                url = `employee_timetable/${user.Data.employee_id}`;
            }
            let response = await calender(url);

            if (response.status) {
                this.setState({ timetable: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.timetable)
            console.log('response: ', this.state.timetable);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View style={{ borderWidth: 2, borderColor: COLORS.primary, marginBottom: 15, borderRadius: 8 }}>
                                    <Picker
                                        selectedValue={this.state.day}
                                        style={{ height: 50, padding: 0, borderWidth: 1, borderColor: COLORS.primary }}
                                        onValueChange={day => this.setState({ day })}
                                    >
                                        {
                                            this.state.days && this.state.days.length && this.state.days.map((row, i) => {
                                                return (
                                                    <Picker.Item label={row} value={row} />
                                                )
                                            })
                                        }
                                    </Picker>
                                </View>
                                {
                                    this.state.timetable && this.state.timetable.data && this.state.timetable.data.filter(item => item.working_day.includes(this.state.day)).length ?
                                        <>
                                            {
                                                this.state.timetable && this.state.timetable.data && this.state.timetable.data.filter(item => item.working_day.includes(this.state.day)).map((row, i) => {
                                                    return (
                                                        <>
                                                            {
                                                                row && row.time_table && row.time_table.length && row.time_table.map((tt, i) => {
                                                                    return (
                                                                        <View style={style.Row}>
                                                                            <View key={i} style={style.Column}>
                                                                                <View>
                                                                                    <Text style={{ color: COLORS.primary, fontSize: 16 }}>{tt.time}</Text>
                                                                                    {
                                                                                        this.state.user.UserType == 'S' ?
                                                                                            <Text>{tt.subject}</Text>
                                                                                            :
                                                                                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                                <Text style={{ flex: .5, fontSize: 16 }}>{tt.subject}</Text>
                                                                                                <Text style={{ flex: .5, fontSize: 16, textAlign: 'right' }}>{tt.class}</Text>
                                                                                            </View>
                                                                                    }
                                                                                </View>
                                                                            </View>
                                                                        </View>
                                                                    )
                                                                })
                                                            }
                                                        </>
                                                    )
                                                })
                                            }
                                        </>
                                        :
                                        <View>
                                            <Text style={{ textAlign: 'center' }}>No record found.</Text>
                                        </View>
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}