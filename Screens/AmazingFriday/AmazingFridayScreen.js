import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, View } from "react-native";
import { view_color_fridays } from "../../Api/menu";
import { COLORS } from "../../configs/constants.config";
import { styles } from "./style";

export default class AmazingFridayScreen extends Component {
    state = {
        fridays: null,
        loading: true
    }
    componentDidMount = async () => {
        let response = await view_color_fridays();

        if (response.status) {
            this.setState({ fridays: response.data.response ?? null, loading: false });
        }
    }
    render() {
        if (this.state.fridays)
            console.log('response: ', this.state.fridays.all_fridays[0].menu);
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View>
                                    <Text style={styles.MainHeading}>{this.state.fridays.color_notes}</Text>
                                </View>
                                {
                                    this.state.fridays && this.state.fridays.all_fridays && this.state.fridays.all_fridays.map((row, i) => {
                                        console.log('menu: ', row);
                                        return (
                                            <View style={styles.Row}>
                                                <View key={i} style={styles.Column}>
                                                    <View>
                                                        <View>
                                                            <Text style={{ color: COLORS.primary }}>{row.date}</Text>
                                                            <Text >{row.description}</Text>
                                                        </View>
                                                        {/* <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ flex: .8 }}>{row.description}</Text>
                                                            <Text style={{ flex: .2, color: COLORS.primary }}>
                                                                {row.date}
                                                            </Text>
                                                        </View> */}
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    })
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}