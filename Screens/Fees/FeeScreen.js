import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Button, Linking, ScrollView, StyleSheet, Text, View } from "react-native";
import { fee } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Attendance/style";

export default class FeeScreen extends Component {
    state = {
        fee: null,
        loading: true,
        user: null,
        integration: null
    }
    componentDidMount = async () => {
        let integration = await AsyncStorage.getItem('@integration');
        integration = JSON.parse(integration);

        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user, integration });
            let url = `fees/${user.Data.student_id}/${user.Data.section_id}`;
            let response = await fee(url);

            if (response.status) {
                this.setState({ fee: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {

        if (this.state.fee)
            console.log('response: ', this.state.fee);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <>
                                <View>
                                    <View style={style.Row}>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>DATE</Text>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>STATUS</Text>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1 }}>AMOUNT</Text>
                                    </View>
                                </View>
                                <View>
                                    {
                                        this.state.fee && this.state.fee.data && this.state.fee.data.map((row, i) => {
                                            return (
                                                <View style={style.Row}>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{row.date}</Text>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1 }}>{row.status}</Text>
                                                    <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>{row.amount} ₹</Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                                <View style={{ marginTop: 10 }}>
                                    <Button
                                        onPress={() => Linking.openURL(this.state.integration.fees_url)}
                                        title="Pay Online Fees"
                                        color={COLORS.primary}
                                    />
                                </View>
                            </>

                    }
                </View>
            </ScrollView >
        );
    }
}