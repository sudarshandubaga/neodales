import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { COLORS } from "../../configs/constants.config";
import { styles } from "./style";

export default class PersonalScreen extends Component {
    state = {
        user: null,
        loading: true,
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user.Data);
            this.setState({
                user: user,
                loading: false,
            });
        }
    }
    render() {
        let user = this.state.user;
        return (
            <ScrollView>

                {
                    this.state.loading
                        ?
                        <View style={{ padding: 10 }}>
                            <ActivityIndicator size="large" color={COLORS.primary} />
                        </View>
                        :
                        <>
                            <View style={{ backgroundColor: COLORS.primary, padding: 30 }}></View>
                            <View style={{ padding: 30 }}></View>
                            <View style={styles.container}>
                                <View>
                                    <View style={{ top: -130 }}>
                                        <Image
                                            source={{ uri: user.Data.photo }}
                                            resizeMode="contain"
                                            style={{ height: 110, borderRadius: 100, }}
                                        />
                                    </View>
                                    <View style={{ top: -120 }}>
                                        <View style={styles.CommanDiv}>
                                            <Text style={{ fontSize: 20, color: COLORS.primary, textAlign: 'center' }}>{user.Data.name}</Text>
                                        </View>
                                        {
                                            this.state.user && user.UserType == 'S' ?
                                                <>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>ENROLL NO. :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.enrollment_number ? user.Data.enrollment_number : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>CLASS :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.class ? user.Data.class : '-'}</Text>
                                                        </View>
                                                    </View>
                                                </>
                                                :
                                                <>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>ATTD. CODE :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.attendace_code ? user.Data.attendace_code : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>DESIGNATION :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.profile ? user.Data.profile : '-'}</Text>
                                                        </View>
                                                    </View>
                                                </>
                                        }
                                        <View style={styles.CommanDiv}>
                                            <View style={styles.Row}>
                                                <Text style={{ flex: 5, color: COLORS.primary }}>FATHER`S NAME :</Text>
                                                <Text style={{ flex: 5 }}>{user.Data.father ? user.Data.father : '-'}</Text>
                                            </View>
                                        </View>
                                        {
                                            this.state.user && user.UserType == 'S' ?
                                                <>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>FATHER`S MOB. :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.mobile ? user.Data.mobile : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>MOTHER`S NAME :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.mother ? user.Data.mother : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>MOTHER`S MOB. :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.mother_mobile ? user.Data.mother_mobile : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>CATEGORY :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.category ? user.Data.category : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>ROLL NO. :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.roll ? user.Data.roll : '-'}</Text>
                                                        </View>
                                                    </View>
                                                </>
                                                :
                                                <>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>MOBILE :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.email ? user.Data.mobile : '-'}</Text>
                                                        </View>
                                                    </View>
                                                </>
                                        }
                                        <View style={styles.CommanDiv}>
                                            <View style={styles.Row}>
                                                <Text style={{ flex: 5, color: COLORS.primary }}>EMAIL :</Text>
                                                <Text style={{ flex: 5 }}>{user.Data.email ? user.Data.email : '-'}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.CommanDiv}>
                                            <View style={styles.Row}>
                                                <Text style={{ flex: 5, color: COLORS.primary }}>DATE OF BIRTH :</Text>
                                                <Text style={{ flex: 5 }}>{user.Data.date_of_birth ? user.Data.date_of_birth : '-'}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.CommanDiv}>
                                            <View style={styles.Row}>
                                                <Text style={{ flex: 5, color: COLORS.primary }}>DATE OF JOINING :</Text>
                                                <Text style={{ flex: 5 }}>{user.Data.date_of_joining ? user.Data.date_of_joining : '-'}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.CommanDiv}>
                                            <View style={styles.Row}>
                                                <Text style={{ flex: 5, color: COLORS.primary }}>ADDRESS. :</Text>
                                                <Text style={{ flex: 5 }}>{user.Data.address ? user.Data.address : '-'}</Text>
                                            </View>
                                        </View>
                                        {
                                            this.state.user && user.UserType == 'S' ?
                                                <>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>PHONE NO. :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.phone ? user.Data.phone : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>PHONE NO. WORK :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.phone_work ? user.Data.phone_work : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>RELIGION :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.religion ? user.Data.religion : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>CASTE :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.caste ? user.Data.caste : '-'}</Text>
                                                        </View>
                                                    </View>
                                                    <View style={styles.CommanDiv}>
                                                        <View style={styles.Row}>
                                                            <Text style={{ flex: 5, color: COLORS.primary }}>B.C. :</Text>
                                                            <Text style={{ flex: 5 }}>{user.Data.bc ? user.Data.bc : '-'}</Text>
                                                        </View>
                                                    </View>
                                                </>
                                                : <>
                                                </>
                                        }
                                    </View>
                                </View>
                            </View>
                        </>
                }
            </ScrollView>
        );
    }
}