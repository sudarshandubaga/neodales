import { StyleSheet } from "react-native";
import { COLORS, SCREEN } from "../../configs/constants.config";

export const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    Row: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    Column: {
        flex: 5
    },
    Heading: {
        fontSize: 18,
        color: COLORS.primary,
    },
    MainHeading: {
        borderBottomWidth: 2,
        borderColor: COLORS.primary,
        borderColor: COLORS.primary,
        fontSize: 20,
        color: COLORS.primary,
        marginBottom: 20,
    },
    CommanDiv: {
        paddingBottom: 10,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderColor: COLORS.primary
    }
});