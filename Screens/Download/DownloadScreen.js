import React, { Component } from "react";
import { ActivityIndicator, Image, Linking, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { view_downloads } from "../../Api/menu";
import { COLORS } from "../../configs/constants.config";
import { styles } from "./style";

export default class DownloadScreen extends Component {
    state = {
        downloads: null,
        loading: true,
    }
    componentDidMount = async () => {
        let response = await view_downloads();

        if (response.status) {
            this.setState({ downloads: response.data.response ?? null, loading: false });
            this.setState({ loading: false });
        }
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }
    render() {
        let { navigation } = this.props;
        if (this.state.downloads)
            console.log('response: ', this.state.downloads[0]);
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                {
                                    this.state.downloads && this.state.downloads.map((row, i) => {
                                        return (
                                            <TouchableOpacity onPress={() => Linking.openURL(row.file_url)}>
                                                <View style={{ paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderColor: COLORS.primary }}>
                                                    <Text>{row.title}</Text>
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    })
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}