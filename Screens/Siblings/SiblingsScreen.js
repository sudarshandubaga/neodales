import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import ApiExecute from "../../Api";
import { siblings } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import { styles } from "./style";

export default class SiblingsScreen extends Component {
    state = {
        user: null,
        loading: true,
        siblings: []
    }

    siblingsLogin = async userId => {
        let response = await ApiExecute(`sibling_login/${userId}`);
        // console.log('login response: ', response);

        if (response.status) {
            await AsyncStorage.setItem('@user', JSON.stringify(response.data.response));
            setTimeout(() => {
                // this.goTo('Home');
                this.props.onLoginSuccess(JSON.stringify(response.data.response));
                this.setState({ loading: false });
            }, 30);
        }
    }

    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            // console.log('user details', user.Data);
            this.setState({
                user: user,
                loading: false,
            });

            let url = `siblings/${user.Data.student_id}/${user.Data.section_id}`;

            let response = await siblings(url);

            if (response.status) {
                this.setState({ siblings: response.data.response.data ?? null, loading: false });
            }
        }
    }
    render() {
        let user = this.props.user;
        return (
            <ScrollView>
                {
                    this.state.loading
                        ?
                        <View style={{ padding: 10 }}>
                            <ActivityIndicator size="large" color={COLORS.primary} />
                        </View>
                        :
                        <View style={styles.Row}>
                            {
                                this.state.siblings.map((row, i) => {
                                    return (
                                        <TouchableOpacity key={i} style={styles.Column} onPress={() => this.siblingsLogin(row.sr_no)}>
                                            <Image
                                                source={{ uri: row.image }}
                                                resizeMode="contain"
                                                style={styles.image}
                                            />
                                            <Text style={styles.SiblingText}>{row.name}</Text>
                                        </TouchableOpacity>
                                    );
                                })
                            }
                        </View>
                }
            </ScrollView>
        );
    }
}