import { StyleSheet } from "react-native";
import { COLORS, SCREEN } from "../../configs/constants.config";

export const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    Row: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    Column: {
        width: '50%',
        padding: 15,
        textAlign: 'center',
        justifyContent: 'center',
        marginBottom: 10
    },
    image: {
        width: 128,
        height: 128,
        alignSelf: 'center',
    },
    SiblingText: {
        marginTop: 5,
        textAlign: 'center'
    }
});