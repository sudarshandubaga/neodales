import React, { Component } from "react";
import { ActivityIndicator, ScrollView, StyleSheet, Text, View } from "react-native";
import { view_menus } from "../../Api/menu";
import { COLORS } from "../../configs/constants.config";
import style from "./style";

export default class MenuScreen extends Component {
    state = {
        menus: null,
        loading: true
    }
    componentDidMount = async () => {
        let response = await view_menus();

        if (response.status) {
            this.setState({ menus: response.data.response ?? null, loading: false });
        }
    }
    render() {
        if (this.state.menus) {
            console.log('response: ', this.state.menus.menu_list[0].menu);
            return (
                <ScrollView>
                    <View style={style.container}>
                        {
                            this.state.loading
                                ?
                                <View style={{ padding: 10 }}>
                                    <ActivityIndicator size="large" color={COLORS.primary} />
                                </View>
                                :
                                <View>
                                    <View>
                                        <Text style={style.MainHeading}>{this.state.menus.menu_month}</Text>
                                    </View>
                                    {
                                        this.state.menus && this.state.menus.menu_list && this.state.menus.menu_list.map((row, i) => {
                                            console.log('menu: ', row);
                                            return (
                                                <View style={style.Row}>
                                                    <View key={i} style={style.Column}>
                                                        <View>
                                                            <Text style={style.Heading}>{row.menu_day}</Text>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Text style={{ flex: .2 }}>MENU : </Text>
                                                                <Text style={{ flex: .8 }}>{row.menu}</Text>
                                                            </View>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Text style={{ flex: .2 }}>TIT-BIT : </Text>
                                                                <Text style={{ flex: .8 }}>{row.snacks}</Text>
                                                            </View>
                                                        </View>
                                                    </View>
                                                </View>
                                            )
                                        })
                                    }
                                </View>

                        }
                    </View>
                </ScrollView>
            );
        } else {
            return (
                <View style={style.container}>
                    <Text>No Records Found.</Text>
                </View>
            );
        }
    }
}