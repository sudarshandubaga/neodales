import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Image, Picker, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { assignment } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import { styles } from "../Event/style";

export default class AssignmentScreen extends Component {
    state = {
        user: null,
        assignments: null,
        loading: true,
        subject: 'All',
        subjects: null,
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `assignments/${user.Data.student_id}/${user.Data.section_id}`;
            let response = await assignment(url);

            if (response.status) {
                console.log('response', response.data.response.data);
                this.setState({ assignments: response.data ?? null, loading: false });
                if (response.data.response_code) {
                    console.log('1st stage');
                    if (response.data.response.data.length) {
                        console.log('2nd stage');
                        let subjects = [];
                        response.data.response.data.forEach(function (row, i) {
                            if (!subjects.includes(row.subject)) {
                                subjects.push(row.subject);
                            }
                        });
                        this.setState({ subjects: subjects });
                        console.log('subjects', subjects);
                    }
                }
            }
        }
    }
    subjectAssignments = async (name = null) => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `assignments_by_subject/${user.Data.student_id}/${user.Data.section_id}/${name}`;

            let response = await assignment(url);

            if (response.status) {
                console.log('response', response.data);
                this.setState({ assignments: response.data ?? null, loading: false });
            }
        }
    }
    render() {
        let { navigation } = this.props;
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View style={{ borderWidth: 2, borderColor: COLORS.primary, marginBottom: 15, borderRadius: 8 }}>
                                    <Picker
                                        selectedValue={this.state.subject}
                                        style={{ height: 50, padding: 0, borderWidth: 1, borderColor: COLORS.primary }}
                                        onValueChange={subject => this.setState({ subject })}
                                    >
                                        <Picker.Item label="All" value="All" />
                                        {
                                            this.state.subjects && this.state.subjects.map((row, i) => {
                                                return (
                                                    <Picker.Item label={row} value={row} />
                                                )
                                            })
                                        }
                                    </Picker>
                                </View>
                                {
                                    this.state.assignments && this.state.assignments.response_code ?
                                        <>
                                            {
                                                this.state.subject == 'All' ?
                                                    <>
                                                        {
                                                            this.state.assignments && this.state.assignments.response && this.state.assignments.response.data && this.state.assignments.response.data.map((row, i) => {
                                                                return (
                                                                    <View style={styles.Row}>
                                                                        <View style={styles.Column} >
                                                                            <TouchableOpacity onPress={() => navigation.push('AssignmentDetails', row)}>
                                                                                <Text style={{ fontSize: 18, color: COLORS.primary, paddingLeft: 10, paddingRight: 10 }}>{row.title}</Text>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Teacher :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.teacher}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Subject :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.subject}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Assign Date :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.created_date}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Submit Date :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.due_date}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Status :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.status}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Remarks :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.remarks}</Text>
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    </View>
                                                                )
                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <>
                                                        {
                                                            this.state.assignments && this.state.assignments.response && this.state.assignments.response.data && this.state.assignments.response.data.filter(item => item.subject.includes(this.state.subject)).map((row, i) => {
                                                                return (
                                                                    <View style={styles.Row}>
                                                                        <View style={styles.Column} >
                                                                            <TouchableOpacity onPress={() => navigation.push('AssignmentDetails', row)}>
                                                                                <Text style={{ fontSize: 18, color: COLORS.primary, paddingLeft: 10, paddingRight: 10 }}>{row.title}</Text>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10, paddingRight: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Teacher :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.teacher}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Subject :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.subject}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Assign Date :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.created_date}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Submit Date :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.due_date}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Status :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.status}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', paddingLeft: 10 }}>
                                                                                    <Text style={{ flex: 5 }}>Remarks :</Text>
                                                                                    <Text style={{ flex: 5 }}>{row.remarks}</Text>
                                                                                </View>
                                                                            </TouchableOpacity>
                                                                        </View>
                                                                    </View>
                                                                )
                                                            })
                                                        }
                                                    </>
                                            }
                                        </>
                                        :
                                        <View>
                                            <Text style={{ textAlign: 'center', paddingTop: 10 }}>{this.state.assignments.response_message}</Text>
                                        </View>
                                }
                            </View>

                    }
                </View>
            </ScrollView >
        );
    }
}