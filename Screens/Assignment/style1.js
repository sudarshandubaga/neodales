import { StyleSheet } from "react-native";
import { COLORS, SCREEN } from "../../configs/constants.config";

export const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    Row: {
        flexDirection: 'row',
        justifyContent: 'center',
        flexWrap: 'wrap',
    },
    Column: {
        marginBottom: 20,
    },
    Heading: {
        fontSize: 18,
        color: COLORS.primary,
    },
    MainHeading: {
        borderBottomWidth: 2,
        borderColor: COLORS.primary,
        borderColor: COLORS.primary,
        fontSize: 20,
        color: COLORS.primary,
        marginBottom: 20,
    },
    btnPrimary: {
        backgroundColor: COLORS.primary,
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 25,
        justifyContent: 'center'
    }
});