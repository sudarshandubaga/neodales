import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Image, Linking, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { assignment } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import { styles } from "../Event/style1";

export default class AssignmentDetailsScreen extends Component {
    state = {
        assignment: null,
        loading: true,
        user: null,
    }
    componentDidMount = async () => {
        let { route } = this.props;
        console.log('route', route.params);
        let assignment_detail = route.params;

        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `assignmentView/${user.Data.student_id}/${user.Data.section_id}/${assignment_detail.id}`;
            console.log('url', url);
            let response = await assignment(url);

            if (response.status) {
                this.setState({ assignment: response.data.response.data ?? null, loading: false });
            }
        }
    }
    render() {
        let assignment = this.state.assignment;
        console.log('assignment', assignment);
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View style={{ paddingBottom: 10 }}>
                                    <Text style={{ fontSize: 18, color: COLORS.primary }}>{assignment.title}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderColor: COLORS.primary }}>
                                    <Text style={{ flex: 5 }}>Submission Date</Text>
                                    <Text style={{ flex: 5, textAlign: 'right' }}>{assignment.due_date}</Text>
                                </View>
                                <View style={{ paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderColor: COLORS.primary }}>
                                    <Text>{assignment.assignment}</Text>
                                </View>
                                <View>
                                    <TouchableOpacity style={styles.btnPrimary} onPress={() => Linking.openURL(assignment.assignmentAttachmentPath)}>
                                        <Text style={{ color: COLORS.white, textAlign: 'center', fontSize: 17 }}>Download Attachment</Text>
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <Text style={{ textAlign: 'center', fontSize: 17, marginTop: 20, marginBottom: 10 }}>Submit Assignment</Text>
                                    <TouchableOpacity style={styles.btnPrimary} onPress={() => ('')}>
                                        <Text style={{ color: COLORS.white, textAlign: 'center', fontSize: 17 }}>Upload Image / File</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                    }
                </View>
            </ScrollView>
        );
    }
}