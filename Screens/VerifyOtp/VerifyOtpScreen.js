import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Alert, ColorPropType, Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { applyleave } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Leave/style";

export default class VerifyOtpScreen extends Component {
    state = {
        user: null,
        loading: true,
        otp: null,
        userOtp: null,
        userName: null,
    }
    componentDidMount = async () => {

        let { route } = this.props;
        console.log('route', route.params);

        this.setState({ userOtp: route.params.otp });
        this.setState({ userName: route.params.username });

        this.setState({ loading: false });
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }
    }
    sendOtp = async () => {
        let user = this.state.user;

        let {
            otp,
            userOtp,
            userName
        } = this.state;

        if (otp == null) {
            Alert.alert(
                "Error",
                'Please enter otp.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else if (userOtp != otp) {
            Alert.alert(
                "Error",
                'Please enter valid otp.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else {
            let url = `forgot_password/${userName}`;
            let response = await applyleave(url);
            console.log('url', response);
            if (response.status) {
                Alert.alert(
                    "Alert",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
                if (response.data.response_code) {
                    this.setState({ otp: null });
                    this.setState({ userOtp: null });
                    this.setState({ userName: null });
                    this.goTo('Login');
                }
            }
        }
    }
    goTo = path => {
        let { navigation } = this.props;
        navigation.navigate(path);
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }
    render() {
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View style={{}}>
                                <View style={{ position: 'absolute', top: 40, left: 120, borderRadius: 100, borderWidth: 1, borderColor: COLORS.primary, backgroundColor: COLORS.white, zIndex: 1 }}>
                                    <View style={{ height: 110, width: 110 }}>
                                    </View>
                                </View>
                                <View style={{ borderWidth: 1, padding: 20, paddingTop: 30, marginTop: 100, borderColor: COLORS.primary, borderRadius: 8 }}>
                                    <View style={{ paddingTop: 40 }}>
                                        <View style={style.formGroup}>
                                            <TextInput style={style.textInput} placeholder="Otp" value={this.state.otp} onChangeText={otp => this.setState({ otp })} placeholderTextColor={COLORS.primary} />
                                        </View>
                                        <View style={{ paddingBottom: 10 }}></View>
                                        <TouchableOpacity style={style.btnPrimary} onPress={() => this.sendOtp()}>
                                            <Text style={{ color: COLORS.white, textAlign: 'center' }}>Recover Password</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <Text style={{ textAlign: 'center', paddingTop: 10 }}>PASSWORD WILL BE SENT ON YOUR REGISTERED MOBILE NUMBER</Text>
                                    </View>
                                </View>
                            </View>
                    }
                </View>
            </ScrollView >
        );
    }
}