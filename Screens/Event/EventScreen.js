import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { view_events } from "../../Api/menu";
import { COLORS } from "../../configs/constants.config";
import { styles } from "./style";

export default class EventScreen extends Component {
    state = {
        events: null,
        loading: true,
    }
    componentDidMount = async () => {
        let response = await view_events();

        if (response.status) {
            this.setState({ events: response.data.response ?? null, loading: false });
            this.setState({ loading: false });
        }
    }
    render() {
        let { navigation } = this.props;
        if (this.state.events)
            console.log('response: ', this.state.events[0]);
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View>
                                    <Text style={styles.MainHeading}>Events</Text>
                                </View>
                                {
                                    this.state.events && this.state.events.map((row, i) => {
                                        return (
                                            <View style={styles.Row}>
                                                <View style={styles.Column} >
                                                    <TouchableOpacity style={[style.Column]} style={{ flexDirection: 'row' }} onPress={() => navigation.push('EventDetails', row)}>
                                                        <Image
                                                            source={{ uri: row.image_url }}
                                                            resizeMode="cover"
                                                            style={{ flex: .4, height: 80 }}
                                                        />
                                                        <View style={{ flex: .6, paddingLeft: 10 }}>
                                                            <Text style={{ fontSize: 18 }}>{row.title}</Text>
                                                            <Text>{row.date}</Text>
                                                            <Text>{row.event_for}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        )
                                    })
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}