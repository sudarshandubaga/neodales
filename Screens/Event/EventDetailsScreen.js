import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { COLORS } from "../../configs/constants.config";
import { styles } from "./style1";

export default class EventDetailsScreen extends Component {
    state = {
        event: null,
        loading: true,
    }
    componentDidMount = async () => {
        console.log('props', this.props);
        this.setState({ loading: false });
    }
    render() {
        let { route } = this.props;
        console.log('route', route.params);
        let event = route.params;
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View>
                                    <Image
                                        source={{ uri: event.image_url }}
                                        resizeMode="contain"
                                        style={{ height: 350, marginBottom: 10 }}
                                    />
                                </View>
                                <View style={{ paddingBottom: 10 }}>
                                    <Text style={{ fontSize: 18, color: COLORS.primary }}>{event.title}</Text>
                                </View>
                                <View style={{ paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderColor: COLORS.primary }}>
                                    <Text>{event.date}</Text>
                                </View>
                                <View style={{ paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderColor: COLORS.primary }}>
                                    <Text>{event.event_for}</Text>
                                </View>
                                <View style={{ paddingBottom: 10, marginBottom: 10, borderBottomWidth: 1, borderColor: COLORS.primary }}>
                                    <Text>{event.description}</Text>
                                </View>
                            </View>
                    }
                </View>
            </ScrollView>
        );
    }
}