import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component, useState } from "react";
import { ActivityIndicator, Alert, Button, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { applyleave } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "./style";
import DatePicker from 'react-native-date-picker'
import moment from "moment";

export default class LeaveScreen extends Component {
    state = {
        user: null,
        menus: null,
        loading: true,
        from_date: '',
        to_date: '',
        reason: null,
        msg: null,
        open: false,
        open1: false,
    }
    componentDidMount = async () => {
        this.setState({ from_date: new Date() });
        this.setState({ to_date: new Date() });

        this.setState({ loading: false });
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }
    }
    applyLeave = async () => {
        let user = this.state.user;

        let {
            from_date,
            to_date,
            reason
        } = this.state;

        if (from_date == null) {
            this.setState({ msg: 'Please select from date.' });
            Alert.alert(
                "Alert",
                'Please select from date.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else if (to_date == null) {
            this.setState({ msg: 'Please select to date.' });
            Alert.alert(
                "Alert",
                'Please select to date.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else if (reason == null) {
            this.setState({ msg: 'Please enter reason.' });
            Alert.alert(
                "Alert",
                'Please enter reason.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else {
            var user_to_date = moment(to_date).format('yyyy-mm-dd')
            var user_from_date = moment(from_date).format('yyyy-mm-dd')

            this.setState({ loading: true });
            let url
            console.log('user info: ', user);
            if (user.UserType == 'S') {
                var studentId = user.StudentData.Data.enrollment_number;
                var className = user.StudentData.SisClassMaster.name + " - " + user.StudentData.SisClassSectionTran.name;
                var studentName = user.StudentData.User.first_name + " " + user.StudentData.User.middle_name + " " + user.StudentData.User.last_name;
                var email = user.Data.email
                // studentId+"/"+studentName+"/"+className+"/"+$scope.from_date+"/"+$scope.to_date+"/"+$scope.reason+"/"+email
                url = `apply_leave/${studentId}/${studentName}/${className}/${user_from_date}/${user_to_date}/${reason}/${email}`;
            } else {
                url = `apply_leave_staff/${user.Data.name}/${user.Data.profile}/${user_from_date}/${user_to_date}/${this.state.reason}/${user.Data.email}`;
            }
            let response = await applyleave(url);
            if (response.data.response_code) {
                // console.log('url', response.data.response_message);
                Alert.alert(
                    "Alert",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
                this.setState({ from_date: new Date(), to_date: new Date(), reason: null, msg: null, loading: false });
            }
        }
    }
    setOpen = async (status) => {
        this.setState({ open: status });
    }
    setDate = async (date) => {
        console.log('date', date);
        this.setState({ from_date: date });
    }
    setOpen1 = async (status) => {
        this.setState({ open1: status });
    }
    setDate1 = async (date) => {
        console.log('date', date);
        this.setState({ to_date: date });
    }
    render() {
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                {/* <Button title="Open" onPress={() => this.setOpen(true)} /> */}
                                <DatePicker
                                    modal
                                    mode="date"
                                    minimumDate={moment(new Date()).subtract(1, 'day')}
                                    format="YYYY-MM-DD"
                                    open={this.state.open}
                                    date={this.state.from_date}
                                    onConfirm={(date) => {
                                        this.setOpen(false)
                                        this.setDate(date)
                                    }}
                                    onCancel={() => {
                                        this.setOpen(false)
                                    }}
                                />
                                <DatePicker
                                    modal
                                    mode="date"
                                    minimumDate={moment(this.state.to_date).subtract(1, 'day')}
                                    format="YYYY-MM-DD"
                                    open={this.state.open1}
                                    date={this.state.to_date}
                                    onConfirm={(date) => {
                                        this.setOpen1(false)
                                        this.setDate1(date)
                                    }}
                                    onCancel={() => {
                                        this.setOpen1(false)
                                    }}
                                />
                                <Text style={{ color: 'red', paddingBottom: 5, display: this.state.msg == null ? 'none' : 'flex' }}>{this.state.msg}</Text>
                                <View style={style.formGroup}>
                                    <TouchableOpacity onPress={() => this.setOpen(true)}>
                                        {/* <TextInput style={style.textInput} placeholder="From Date" value={this.state.from_date} placeholderTextColor={COLORS.primary} /> */}
                                        <Text style={{ borderWidth: 1, padding: 10, borderColor: COLORS.primary, borderRadius: 100, color: COLORS.primary }}> {moment(this.state.from_date).format('D MMMM Y')} </Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={style.formGroup}>
                                    <TouchableOpacity onPress={() => this.setOpen1(true)}>
                                        <Text style={{ borderWidth: 1, padding: 10, borderColor: COLORS.primary, borderRadius: 100, color: COLORS.primary }}> {moment(this.state.to_date).format('D MMMM Y')} </Text>
                                        {/* <TextInput name="date" style={style.textInput} placeholder="To Date" value={this.state.to_date} onChangeText={to_date => this.setState({ to_date })} placeholderTextColor={COLORS.primary} /> */}
                                    </TouchableOpacity>
                                </View>
                                <View style={style.formGroup}>
                                    <TextInput
                                        style={style.textInput, { borderWidth: 1, padding: 15, borderColor: COLORS.primary, borderRadius: 20, textAlignVertical: "top" }}
                                        placeholder="Leave Reason"
                                        multiline={true}
                                        numberOfLines={4}
                                        value={this.state.reason}
                                        onChangeText={reason => this.setState({ reason })}
                                        placeholderTextColor={COLORS.primary}
                                    />
                                </View>
                                <TouchableOpacity style={style.btnPrimary} onPress={() => this.applyLeave()}>
                                    <Text style={{ color: COLORS.white, textAlign: 'center' }}>Submit</Text>
                                </TouchableOpacity>
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}