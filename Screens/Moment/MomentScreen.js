import React, { Component } from "react";
import { ActivityIndicator, FlatList, Image, ScrollView, Text, TouchableOpacity, TouchableWithoutFeedback, View } from "react-native";
import { view_gallery } from "../../Api/menu";
import { COLORS, SCREEN } from "../../configs/constants.config";
import style from "./style";
import ImageView from 'react-native-image-view';

export default class MomentScreen extends Component {
    state = {
        gallery: null,
        loading: true,
        isImageViewVisible: false,
        images: [],
        imageIndex: 0,
    }
    componentDidMount = async () => {
        let response = await view_gallery();

        if (response.status) {
            this.setState({ gallery: response.data.response ?? null, loading: false });
            if (response.data.response_code) {
                let item = [];
                response.data.response.forEach((element, index) => {
                    item[index] = {
                        source: {
                            uri: element.file_url,
                        },
                    }
                });
                console.log('item', item);
                this.setState({ images: item });
            }
        }
    }
    changeImage = async (index = null) => {
        if (index) {
            this.setState({ imageIndex: index });
        }
        if (this.state.isImageViewVisible) {
            this.setState({ isImageViewVisible: false });
        } else {
            this.setState({ isImageViewVisible: true });
        }
    }

    render() {
        let { navigation } = this.props;
        if (this.state.gallery)
            console.log('response: ', this.state.gallery[0].file_url);
        return (
            <ScrollView>
                <View style={{ padding: 10 }}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <ImageView
                                    images={this.state.images}
                                    onPress={() => this.changeImage()}
                                    onClose={() => this.changeImage()}
                                    imageIndex={this.state.imageIndex}
                                    animationType={'fade'}
                                    isVisible={this.state.isImageViewVisible}
                                    isSwipeCloseEnabled={true}
                                    isPinchZoomEnabled={true}
                                />

                                <FlatList
                                    data={this.state.gallery}
                                    numColumns={2}
                                    renderItem={({ item, index }) => (
                                        <TouchableOpacity
                                            onPress={() => this.changeImage(index)}
                                            style={{
                                                flex: 1,
                                                flexDirection: 'column',
                                                margin: 10,
                                                justifyContent: 'space-evenly',
                                                backgroundColor: '#fff',
                                                overflow: 'hidden',
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 1,
                                                },
                                                shadowOpacity: 0.20,
                                                shadowRadius: 1.41,
                                                elevation: 2,
                                            }}>
                                            <View
                                            >
                                                <Image
                                                    source={{ uri: item.file_url }}
                                                    resizeMode="contain"
                                                    style={{
                                                        width: SCREEN.width / 2.1 - 11,
                                                        height: 150
                                                    }} />
                                            </View>
                                        </TouchableOpacity>

                                    )}
                                    //Setting the number of column

                                    keyExtractor={(item, index) => index}
                                />
                            </View>

                    }
                </View>
            </ScrollView >
        );
    }
}