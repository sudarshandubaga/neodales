import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, View } from "react-native";
import { view_whatsnew } from "../../Api/menu";
import { COLORS } from "../../configs/constants.config";
import { styles } from "./style";

export default class WhatsNewScreen extends Component {
    state = {
        news: null,
        loading: true
    }
    componentDidMount = async () => {
        let response = await view_whatsnew();

        if (response.status) {
            this.setState({ news: response.data.response ?? null, loading: false });
        }
    }
    render() {
        if (this.state.news)
            console.log('response: ', this.state.news[0].date);
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                {
                                    this.state.news && this.state.news.map((row, i) => {
                                        console.log('holiday: ', row);
                                        return (
                                            <View style={styles.Row}>
                                                <View key={i} style={styles.Column}>
                                                    <View>
                                                        <View>
                                                            <Text >{row.news_description}</Text>
                                                        </View>
                                                        {/* <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ flex: .8 }}>{row.description}</Text>
                                                            <Text style={{ flex: .2, color: COLORS.primary }}>
                                                                {row.date}
                                                            </Text>
                                                        </View> */}
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    })
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}