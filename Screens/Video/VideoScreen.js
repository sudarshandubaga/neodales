import React, { Component } from "react";
import { ActivityIndicator, FlatList, Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { view_videos } from "../../Api/menu";
import { COLORS, SCREEN } from "../../configs/constants.config";
import { WebView } from 'react-native-webview';
import YoutubePlayer from 'react-native-youtube-iframe';
import { Linking } from "react-native";

export default class VideoScreen extends Component {
    state = {
        gallery: null,
        loading: true,
        isReady: false,
        quality: 0,
        error: null,
    }
    componentDidMount = async () => {
        let response = await view_videos();

        if (response.status) {
            console.log('gallery: ', response.data);
            this.setState({ gallery: response.data ?? null, loading: false });
        }
    }
    render() {
        let { navigation } = this.props;
        return (
            <ScrollView>
                <View style={{ padding: 10 }}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                {/* <View style={{ flex: 1 }}>
                                    <WebView
                                        style={{ marginTop: (Platform.OS == 'ios') ? 20 : 0, }}
                                        javaScriptEnabled={true}
                                        domStorageEnabled={true}
                                        // source={{ uri: 'https://www.youtube.com/embed/' + this.state.pictureData.idVideo }}
                                        source={{ uri: 'https://www.youtube.com/embed/kK97QAqH4OI' }}
                                    />
                                </View> */}
                                {/* <View>
                                    <YoutubePlayer
                                        height={300}
                                        play={true}
                                        videoId={'kK97QAqH4OI'}
                                    />
                                </View> */}
                                {
                                    this.state.gallery && this.state.gallery.response_code ?
                                        <FlatList
                                            data={this.state.gallery.response}
                                            numColumns={2}
                                            renderItem={({ item }) => (
                                                <TouchableOpacity
                                                    onPress={() => Linking.openURL(item.url)}
                                                    style={{
                                                        flex: 1,
                                                        flexDirection: 'column',
                                                        // marginRight: 15,
                                                        // marginBottom: 15,
                                                        margin: 10,
                                                        justifyContent: 'space-evenly',
                                                        backgroundColor: '#fff',
                                                        overflow: 'hidden',
                                                        shadowColor: "#000",
                                                        shadowOffset: {
                                                            width: 0,
                                                            height: 1,
                                                        },
                                                        shadowOpacity: 0.20,
                                                        shadowRadius: 1.41,
                                                        elevation: 2,
                                                    }}>
                                                    <View>
                                                        <Image
                                                            source={{ uri: item.image }}
                                                            resizeMode="contain"
                                                            style={{
                                                                width: SCREEN.width / 2.1 - 11,
                                                                height: 150
                                                            }} />
                                                    </View>
                                                </TouchableOpacity>

                                            )}
                                            //Setting the number of column

                                            keyExtractor={(item, index) => index}
                                        />
                                        :
                                        <View>
                                            <Text style={{ textAlign: 'center', paddingTop: 10 }}>{this.state.gallery.response_message}</Text>
                                        </View>
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}