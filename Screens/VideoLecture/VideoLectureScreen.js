import React, { Component } from "react";
import { ActivityIndicator, FlatList, Image, ScrollView, Text, TouchableOpacity, View, Picker } from "react-native";
import { view_videos } from "../../Api/menu";
import { COLORS, SCREEN } from "../../configs/constants.config";
import { WebView } from 'react-native-webview';
import YoutubePlayer from 'react-native-youtube-iframe';
import { video_chapters, video_subjects } from "../../Api/student";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Linking } from "react-native";

export default class VideoLectureScreen extends Component {
    state = {
        user: null,
        subjects: [],
        chapters: [],
        gallery: null,
        loading: true,
        isReady: false,
        quality: 0,
        error: null,
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');

        if (user) {
            user = JSON.parse(user);
            this.setState({ user });

            let url = `lmsVideoSubjects/${user.Data.student_id}/${user.Data.section_id}`;
            let response = await video_subjects(url);

            if (response.status) {
                this.setState({ subjects: response.data.response.data ?? [], loading: false });
            }
        }

    }

    fetchChapters = async (subjectId) => {
        if (subjectId) {
            let user = await AsyncStorage.getItem('@user');

            if (user) {
                user = JSON.parse(user);
                this.setState({ user });
            }

            let url = `lmsVideoSubjectChapters/${user.Data.student_id}/${user.Data.section_id}/${subjectId}`;
            let response = await video_chapters(url);

            this.setState({ loading: true });
            if (response.status) {
                this.setState({ chapters: response.data.response.data ?? [], loading: false });
            }
        } else {
            this.setState({ chapters: [] });
        }
    }

    fetchLectures = async (chapterId) => {
        if (chapterId) {
            let user = await AsyncStorage.getItem('@user');

            if (user) {
                user = JSON.parse(user);
                this.setState({ user });
            }

            let url = `lmsSubjectVideo/${user.Data.student_id}/${user.Data.section_id}/${chapterId}`;
            let response = await video_chapters(url);

            this.setState({ loading: true });

            console.log('video gallery: ', response.data.response);

            if (response.status) {
                this.setState({ gallery: response.data.response ?? [], loading: false });
            }
        } else {
            this.setState({ gallery: [] });
        }
    }

    render() {
        let { navigation } = this.props;
        let subjectItems = [], chapterItems = [];

        subjectItems.push(
            <Picker.Item
                label="Please select subject"
                value={null}
            />
        );

        for (let index in this.state.subjects) {
            subjectItems.push(
                <Picker.Item
                    label={this.state.subjects[index]}
                    value={index}
                />
            );
        }

        chapterItems.push(
            <Picker.Item
                label="Please select chapter"
                value={null}
            />
        );

        for (let index in this.state.chapters) {
            chapterItems.push(
                <Picker.Item
                    label={this.state.chapters[index]}
                    value={index}
                />
            );
        }

        return (
            <View>
                <View style={{ padding: 10 }}>
                    <View>
                        <Picker
                            style={{ marginBottom: 10 }}
                            onValueChange={(itemValue, itemIndex) => this.fetchChapters(itemValue)}
                        >
                            {
                                subjectItems
                            }
                        </Picker>
                        <Picker style={{ marginBottom: 10 }}
                            onValueChange={(itemValue, itemIndex) => this.fetchLectures(itemValue)}
                        >
                            {
                                chapterItems
                            }
                        </Picker>
                    </View>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                {/* <View style={{ flex: 1 }}>
                                    <WebView
                                        style={{ marginTop: (Platform.OS == 'ios') ? 20 : 0, }}
                                        javaScriptEnabled={true}
                                        domStorageEnabled={true}
                                        // source={{ uri: 'https://www.youtube.com/embed/' + this.state.pictureData.idVideo }}
                                        source={{ uri: 'https://www.youtube.com/embed/kK97QAqH4OI' }}
                                    />
                                </View> */}
                                {/* <View>
                                    <YoutubePlayer
                                        height={300}
                                        play={true}
                                        videoId={'kK97QAqH4OI'}
                                    />
                                </View> */}
                                {
                                    this.state.gallery ?
                                        <FlatList
                                            data={this.state.gallery.data.video_data}
                                            numColumns={2}
                                            renderItem={({ item }) => (
                                                <TouchableOpacity
                                                    onPress={() => Linking.openURL(item.LmsVideoMaster.url)}
                                                    style={{
                                                        flex: 1,
                                                        flexDirection: 'column',
                                                        // marginRight: 15,
                                                        // marginBottom: 15,
                                                        margin: 10,
                                                        justifyContent: 'space-evenly',
                                                        backgroundColor: '#fff',
                                                        overflow: 'hidden',
                                                        shadowColor: "#000",
                                                        shadowOffset: {
                                                            width: 0,
                                                            height: 1,
                                                        },
                                                        shadowOpacity: 0.20,
                                                        shadowRadius: 1.41,
                                                        elevation: 2,
                                                    }}>
                                                    <View
                                                        style={{
                                                            textAlign: 'center',
                                                            padding: 10,
                                                            justifyContent: 'center'
                                                        }}
                                                    >
                                                        {/* <Image
                                                            source={{ uri: item.file_url }}
                                                            resizeMode="contain"
                                                            style={{
                                                                width: SCREEN.width / 2.1 - 11,
                                                                height: 150
                                                            }} /> */}
                                                        <Text style={{ textAlign: 'center', marginVertical: 30 }}>Play Now</Text>
                                                        <Text style={{ textAlign: 'center', fontSize: 14, fontWeight: 'bold', textTransform: 'capitalize' }}>{item.LmsVideoTran.title}</Text>
                                                    </View>
                                                </TouchableOpacity>

                                            )}
                                            //Setting the number of column

                                            keyExtractor={(item, index) => index}
                                        />
                                        :
                                        <></>
                                }
                            </View>

                    }
                </View>
            </View>
        );
    }
}