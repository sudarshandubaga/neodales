import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Alert, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { askquestion } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Leave/style";

export default class QuestionScreen extends Component {
    state = {
        user: null,
        loading: true,
        message: null,
        msg: null,
    }
    componentDidMount = async () => {
        this.setState({ loading: false });
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }
    }
    askQuestion = async () => {
        let user = this.state.user;
        let {
            message
        } = this.state;
        if (message == null) {
            this.setState({ msg: 'Please enter message.' });
        } else {
            let url = `ask_question/${user.Data.student_id}/${user.Data.name}/${user.Data.mobile}/${user.Data.class}/${this.state.message}/${user.Data.email}`;
            let response = await askquestion(url);
            if (response.data.response_code) {
                console.log('url', response.data.response_message);
                Alert.alert(
                    "Alert",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
                this.setState({ message: null });
                this.setState({ msg: null });
            }
        }
    }
    render() {
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <Text style={{ color: 'red', paddingBottom: 5, display: this.state.msg == null ? 'none' : 'flex' }}>{this.state.msg}</Text>
                                <View style={style.formGroup}>
                                    <TextInput
                                        style={style.textInput, { height: 100, borderWidth: 1, padding: 10, borderColor: COLORS.primary, borderRadius: 20 }}
                                        placeholder="Message"
                                        value={this.state.message}
                                        onChangeText={message => this.setState({ message })}
                                        placeholderTextColor={COLORS.primary}
                                    />
                                </View>
                                <TouchableOpacity style={style.btnPrimary} onPress={() => this.askQuestion()}>
                                    <Text style={{ color: COLORS.white, textAlign: 'center' }}>Submit</Text>
                                </TouchableOpacity>
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}