import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Picker, ScrollView, StyleSheet, Text, TextInput, View } from "react-native";
import { calender } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Menu/style";

export default class CalenderScreen extends Component {
    state = {
        calender: null,
        loading: true,
        month: 'Oct',
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');

        var month = new Date().getMonth() + 1;
        let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        let month_name = monthNames[month - 1];
        this.setState({ month: month_name });
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url;
            if (user.UserType == 'S') {
                url = `event_calender/${user.Data.student_id}/${user.Data.section_id}`;
            } else {
                url = `event_calender/${user.Data.employee_id}`;
            }
            let response = await calender(url);

            if (response.status) {
                this.setState({ calender: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.calender)
            console.log('response: ', this.state.calender);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View style={{ borderWidth: 2, borderColor: COLORS.primary, marginBottom: 15, borderRadius: 8 }}>
                                    <Picker
                                        selectedValue={this.state.month}
                                        style={{ height: 50, padding: 0, borderWidth: 1, borderColor: COLORS.primary }}
                                        onValueChange={month => this.setState({ month })}
                                    >
                                        <Picker.Item label="All Months" value="All" />
                                        <Picker.Item label="April" value="Apr" />
                                        <Picker.Item label="May" value="May" />
                                        <Picker.Item label="June" value="Jun" />
                                        <Picker.Item label="July" value="Jul" />
                                        <Picker.Item label="August" value="Aug" />
                                        <Picker.Item label="September" value="Sep" />
                                        <Picker.Item label="October" value="Oct" />
                                        <Picker.Item label="November" value="Nov" />
                                        <Picker.Item label="December" value="Dec" />
                                        <Picker.Item label="January" value="Jan" />
                                        <Picker.Item label="February" value="Feb" />
                                        <Picker.Item label="March" value="Mar" />
                                    </Picker>
                                </View>
                                {
                                    this.state.month == 'All' ?
                                        <>
                                            {
                                                this.state.calender && this.state.calender.data && this.state.calender.data.length ?
                                                    <>
                                                        {
                                                            this.state.calender && this.state.calender.data && this.state.calender.data.map((row, i) => {
                                                                return (
                                                                    <View style={style.Row}>
                                                                        <View key={i} style={style.Column}>
                                                                            <View>
                                                                                <Text style={{ color: COLORS.primary, fontSize: 16 }}>{row.date}</Text>
                                                                                <Text>{row.event}</Text>
                                                                                {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16 }}>{row.sender_name}</Text>
                                                        <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16, textAlign: 'right' }}>{row.send_date}</Text>
                                                    </View> */}
                                                                            </View>
                                                                        </View>
                                                                    </View>
                                                                )
                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <View>
                                                        <Text style={{ textAlign: 'center' }}>No record found.</Text>
                                                    </View>
                                            }
                                        </>
                                        :
                                        <>
                                            {
                                                this.state.calender && this.state.calender.data && this.state.calender.data.filter(item => item.date.includes(this.state.month)).length ?
                                                    <>
                                                        {
                                                            this.state.calender && this.state.calender.data && this.state.calender.data.filter(item => item.date.includes(this.state.month)).map((row, i) => {
                                                                return (
                                                                    <View style={style.Row}>
                                                                        <View key={i} style={style.Column}>
                                                                            <View>
                                                                                <Text style={{ color: COLORS.primary, fontSize: 16 }}>{row.date}</Text>
                                                                                <Text>{row.event}</Text>
                                                                                {/* <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                        <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16 }}>{row.sender_name}</Text>
                                                        <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16, textAlign: 'right' }}>{row.send_date}</Text>
                                                    </View> */}
                                                                            </View>
                                                                        </View>
                                                                    </View>
                                                                )
                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <View>
                                                        <Text style={{ textAlign: 'center' }}>No record found.</Text>
                                                    </View>
                                            }
                                        </>
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}