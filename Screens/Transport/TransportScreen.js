import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Button, Linking, ScrollView, StyleSheet, Text, View } from "react-native";
import { transport } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Attendance/style";

export default class TransportScreen extends Component {
    state = {
        transport: null,
        loading: true,
        integration: {}
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `transport/${user.Data.student_id}/${user.Data.section_id}`;
            let response = await transport(url);

            if (response.status) {
                console.log('response.response', response.data.response);
                this.setState({ transport: response.data.response ?? null, loading: false });
            }

            let integration = await AsyncStorage.getItem('@integration');
            this.setState({ integration: JSON.parse(integration) });
        }
    }
    render() {

        if (this.state.transport)
            console.log('response: ', this.state.transport);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <>
                                <View style={{ marginBottom: 0, flexDirection: 'row', padding: 10 }}>
                                    <Text style={{ flex: 5, color: COLORS.primary }}>Route No. :</Text>
                                    <Text style={{ flex: 5, textAlign: 'right' }}>{this.state.transport.route ? this.state.transport.route : '-'}</Text>
                                </View>
                                <View style={{ marginBottom: 20, flexDirection: 'row', padding: 10 }}>
                                    <Text style={{ flex: 5, color: COLORS.primary }}>Destination :</Text>
                                    <Text style={{ flex: 5, textAlign: 'right' }}>{this.state.transport.destination ? this.state.transport.destination : '-'}</Text>
                                </View>
                                <View>
                                    <View style={style.Row}>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>DATE</Text>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1 }}>STATUS</Text>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>AMOUNT</Text>
                                    </View>
                                </View>
                                <View>
                                    {
                                        this.state.transport && this.state.transport.data ?
                                            <>
                                                {
                                                    this.state.transport && this.state.transport.data && this.state.transport.data.map((row, i) => {
                                                        return (
                                                            <View style={style.Row}>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{row.date}</Text>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1 }}>{row.status}</Text>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>{row.amount} ₹</Text>
                                                            </View>
                                                        )
                                                    })
                                                }
                                            </>
                                            :
                                            <View style={style.Row}>
                                                <Text style={{ flex: 10, padding: 10, borderColor: COLORS.primary, borderWidth: 1, textAlign: 'center' }}>No Record Found.</Text>
                                            </View>
                                    }
                                </View>
                                <View style={{ marginTop: 10 }}>
                                    <Button
                                        onPress={() => Linking.openURL(this.state.integration.bus_url)}
                                        title="Track My Bus"
                                        color={COLORS.primary}
                                    />
                                </View>
                            </>

                    }
                </View>
            </ScrollView >
        );
    }
}