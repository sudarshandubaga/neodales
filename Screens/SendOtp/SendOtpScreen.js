import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Alert, ColorPropType, Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { applyleave } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Leave/style";

export default class SendOtpScreen extends Component {
    state = {
        user: null,
        loading: true,
        username: null,
    }
    componentDidMount = async () => {
        this.setState({ loading: false });
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }
    }
    sendOtp = async () => {
        let user = this.state.user;

        let {
            username
        } = this.state;

        if (username == null) {
            Alert.alert(
                "Error",
                'Please enter user name.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else {
            let url = `send_otp/${this.state.username}`;
            let response = await applyleave(url);
            console.log('url', response);
            if (response.status) {
                Alert.alert(
                    "Alert",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
                if (response.data.response_code) {
                    let prop = {
                        otp: response.data.otp,
                        username: username
                    }
                    this.navigateTo('VerifyOtp', prop)
                    this.setState({ username: null });
                }
            }
        }
    }
    goTo = path => {
        let { navigation } = this.props;
        navigation.navigate(path);
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.navigate(path, params);
    }
    render() {
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View style={{}}>
                                <View style={{ position: 'absolute', top: 40, left: 120, borderRadius: 100, borderWidth: 1, borderColor: COLORS.primary, backgroundColor: COLORS.white, zIndex: 1 }}>
                                    <View style={{ height: 110, width: 110 }}>
                                    </View>
                                </View>
                                <View style={{ borderWidth: 1, padding: 20, paddingTop: 30, marginTop: 100, borderColor: COLORS.primary, borderRadius: 8 }}>
                                    <View style={{ paddingTop: 40 }}>
                                        <View style={style.formGroup}>
                                            <TextInput style={style.textInput} placeholder="User Name" value={this.state.username} onChangeText={username => this.setState({ username })} placeholderTextColor={COLORS.primary} />
                                        </View>
                                        <View style={{ paddingBottom: 10 }}></View>
                                        <TouchableOpacity style={style.btnPrimary} onPress={() => this.sendOtp()}>
                                            <Text style={{ color: COLORS.white, textAlign: 'center' }}>Send Otp</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View>
                                        <Text style={{ textAlign: 'center', paddingTop: 10 }}>OTP WILL BE SENT ON YOUR REGISTERED MOBILE NUMBER</Text>
                                    </View>
                                </View>
                            </View>
                    }
                </View>
            </ScrollView >
        );
    }
}