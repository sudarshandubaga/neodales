import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, View } from "react-native";
import { view_holidays } from "../../Api/menu";
import { COLORS } from "../../configs/constants.config";
import { styles } from "./style";

export default class HolidayScreen extends Component {
    state = {
        holidays: null,
        loading: true
    }
    componentDidMount = async () => {
        let response = await view_holidays();

        if (response.status) {
            this.setState({ holidays: response.data.response ?? null, loading: false });
        }
    }
    render() {
        if (this.state.holidays)
            console.log('response: ', this.state.holidays.holidays[0].date);
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View>
                                    <Text style={styles.MainHeading}>Holidays</Text>
                                </View>
                                {
                                    this.state.holidays && this.state.holidays.holidays && this.state.holidays.holidays.map((row, i) => {
                                        console.log('holiday: ', row);
                                        return (
                                            <View style={styles.Row}>
                                                <View key={i} style={styles.Column}>
                                                    <View>
                                                        <View>
                                                            <Text style={{ color: COLORS.primary }}>{row.date}</Text>
                                                            <Text >{row.description}</Text>
                                                        </View>
                                                        {/* <View style={{ flexDirection: 'row' }}>
                                                            <Text style={{ flex: .8 }}>{row.description}</Text>
                                                            <Text style={{ flex: .2, color: COLORS.primary }}>
                                                                {row.date}
                                                            </Text>
                                                        </View> */}
                                                    </View>
                                                </View>
                                            </View>
                                        )
                                    })
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}