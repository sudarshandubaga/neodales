import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Picker, ScrollView, StyleSheet, Text, TextInput, View } from "react-native";
import { ediary } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Menu/style";

export default class EdiaryScreen extends Component {
    state = {
        ediary: null,
        loading: true,
        note_type: 'All',
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `ediary/${user.Data.student_id}/${user.Data.section_id}`;
            let response = await ediary(url);

            if (response.status) {
                this.setState({ ediary: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.ediary)
            console.log('response: ', this.state.ediary);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View style={{ borderWidth: 2, borderColor: COLORS.primary, marginBottom: 15, borderRadius: 8 }}>
                                    <Picker
                                        selectedValue={this.state.note_type}
                                        style={{ height: 50, padding: 0, borderWidth: 1, borderColor: COLORS.primary }}
                                        onValueChange={note_type => this.setState({ note_type })}
                                    >
                                        <Picker.Item label="All" value="All" />
                                        <Picker.Item label="Positive" value="Positive" />
                                        <Picker.Item label="Nagative" value="Nagative" />
                                        <Picker.Item label="Neutral" value="Neutral" />
                                    </Picker>
                                </View>
                                {
                                    this.state.note_type == 'All' ?
                                        <>
                                            {
                                                this.state.ediary && this.state.ediary.data && this.state.ediary.data.length ?
                                                    <>
                                                        {
                                                            this.state.ediary && this.state.ediary.data && this.state.ediary.data.map((row, i) => {
                                                                return (
                                                                    <View style={style.Row}>
                                                                        <View key={i} style={style.Column}>
                                                                            <View>
                                                                                <Text>{row.message_info}</Text>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16 }}>{row.sender_name}</Text>
                                                                                    <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16, textAlign: 'right' }}>{row.send_date}</Text>
                                                                                </View>
                                                                            </View>
                                                                        </View>
                                                                    </View>
                                                                )
                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <View>
                                                        <Text style={{ textAlign: 'center' }}>No record found.</Text>
                                                    </View>
                                            }

                                        </>
                                        :
                                        <>
                                            {
                                                this.state.ediary && this.state.ediary.data && this.state.ediary.data.filter((item) => item.note_type == this.state.note_type).length ?
                                                    <>
                                                        {
                                                            this.state.ediary && this.state.ediary.data && this.state.ediary.data.filter((item) => item.note_type == this.state.note_type).map((row, i) => {
                                                                return (
                                                                    <View style={style.Row}>
                                                                        <View key={i} style={style.Column}>
                                                                            <View>
                                                                                <Text>{row.message_info}</Text>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16 }}>{row.sender_name}</Text>
                                                                                    <Text style={{ flex: .5, color: COLORS.primary, fontSize: 16, textAlign: 'right' }}>{row.send_date}</Text>
                                                                                </View>
                                                                            </View>
                                                                        </View>
                                                                    </View>
                                                                )
                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <View>
                                                        <Text style={{ textAlign: 'center' }}>No record found.</Text>
                                                    </View>
                                            }

                                        </>
                                }
                            </View>

                    }
                </View>
            </ScrollView>
        );
    }
}