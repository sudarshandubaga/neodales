import { StyleSheet } from "react-native";
import { COLORS } from "../../configs/constants.config";

export default style = StyleSheet.create({
    fullScreen: {
        height: '100%',
        backgroundColor: COLORS.white
    },
    loginPart: {
        padding: 15,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    formGroup: {
        marginBottom: 10,
    },
    textInput: {
        height: 40,
        borderColor: COLORS.primary,
        borderWidth: 1,
        color: COLORS.primary,
        paddingHorizontal: 15,
        borderRadius: 30
    },
    btnPrimary: {
        backgroundColor: COLORS.primary,
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 25,
        justifyContent: 'center'
    }
});