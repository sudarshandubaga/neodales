import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Alert, Picker, SafeAreaView, ScrollView, StatusBar, Text, TextInput, TouchableOpacity, View } from "react-native";
import Svg, { Path } from "react-native-svg";
import { login } from "../../Api/login";
import { AppInfo, COLORS, SCREEN } from "../../configs/constants.config";
import style from "./style";

export default class LoginScreen extends Component {
    state = {
        user_name: null,
        password: null,
        msg: null,
        loading: false,
        selectedValue: 'java'
    }

    async componentDidMount() {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            let self = this;
            setTimeout(() => {
                // self.goTo('Home');
            }, 30);
        }
    }
    LoginSubmit = async (e) => {
        let {
            user_name,
            password
        } = this.state;
        if (user_name == null) {
            console.log('Please enter user name.');
            Alert.alert(
                "Error",
                'Please enter user name.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else if (password == null) {
            console.log('Please enter password.');
            Alert.alert(
                "Error",
                'Please enter password.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else {
            this.setState({ loading: true });
            let url = `login/${user_name}/${password}`;
            let response = await login(url);
            if (response.status) {
                console.log('login', response.data);
                if (response.data.response_code) {
                    console.log('response1', response.data);
                    await AsyncStorage.setItem('@user', JSON.stringify(response.data.response));
                    setTimeout(() => {
                        // this.goTo('Home');
                        this.props.onLoginSuccess(JSON.stringify(response.data.response));
                        this.setState({ loading: false });
                    }, 30);
                } else {
                    console.log('response2', response.data.response_message);

                    this.setState({ loading: false });
                    // this.setState({ msg: response.data.response_message });
                    Alert.alert(
                        "Error",
                        response.data.response_message,
                        [
                            { text: "OK", onPress: () => console.log("OK Pressed") }
                        ]
                    );
                    // setTimeout(() => { this.setState({ msg: null }) }, 3000)
                }
                //     this.setState({ holidays: response.data.response ?? null, loading: false });
            }
        }
    }
    goTo = path => {
        let { navigation } = this.props;
        navigation.push(path);
    }
    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.push(path, params);
    }
    render() {
        return (
            <>
                <SafeAreaView style={style.fullScreen}>
                    <ScrollView>
                        <View style={{ backgroundColor: COLORS.primary }}>
                            <View style={{ color: COLORS.white, paddingHorizontal: 10, paddingTop: 145, paddingBottom: 50 }}>
                                <Text style={{ color: COLORS.white, fontSize: 18 }}>
                                    Welcome To
                                </Text>
                                <Text style={{ color: COLORS.white, fontWeight: 'bold', fontSize: 36 }}>
                                    {AppInfo.name}
                                </Text>
                            </View>
                            <Svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" style={{ width: SCREEN.width + 5, height: SCREEN.height * 2 / 9, marginBottom: -60 }}>
                                <Path fill={COLORS.white} fill-opacity="1" d="M0,160L48,176C96,192,192,224,288,213.3C384,203,480,149,576,154.7C672,160,768,224,864,218.7C960,213,1056,139,1152,96C1248,53,1344,43,1392,37.3L1440,32L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z"></Path>
                            </Svg>
                        </View>
                        <View style={style.loginPart}>
                            <Text style={{ color: 'red', paddingBottom: 5, display: this.state.msg == null ? 'none' : 'flex' }}>{this.state.msg}</Text>
                            <View style={style.formGroup}>
                                <TextInput style={style.textInput} placeholder="Username" value={this.state.user_name} onChangeText={user_name => this.setState({ user_name })} placeholderTextColor={COLORS.primary} />
                            </View>
                            <View style={style.formGroup}>
                                <TextInput style={style.textInput} placeholder="Password" value={this.state.password} onChangeText={password => this.setState({ password })} secureTextEntry={true} placeholderTextColor={COLORS.primary} />
                            </View>
                            <TouchableOpacity onPress={() => this.navigateTo('SendOtp')}>
                                <Text style={{ paddingBottom: 10, paddingRight: 10, textAlign: 'right' }}>Forgot Password</Text>
                            </TouchableOpacity>
                            {
                                this.state.loading ?
                                    <View style={style.btnPrimary} onPress={() => ('')}>
                                        <Text style={{ color: COLORS.white, textAlign: 'center' }}>
                                            <ActivityIndicator size="small" color={COLORS.white} />
                                        </Text>
                                    </View>
                                    :
                                    <TouchableOpacity style={style.btnPrimary} onPress={() => this.LoginSubmit()}>
                                        <Text style={{ color: COLORS.white, textAlign: 'center' }}>Login</Text>
                                    </TouchableOpacity>
                            }
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </>
        );
    }
}