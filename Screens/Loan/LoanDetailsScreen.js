import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Image, ScrollView, Text, TouchableOpacity, View } from "react-native";
import { calender } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Attendance/style";
import { styles } from "../Event/style1";

export default class LoanDetailsScreen extends Component {
    state = {
        loan: null,
        loading: true,
    }
    componentDidMount = async () => {
        let { route } = this.props;
        let loanProps = route.params;
        console.log(loanProps);
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `employee_loan_details/${user.Data.employee_id}/${loanProps.loan_id}`;

            let response = await calender(url);

            if (response.status) {
                this.setState({ loan: response.data.response.data ?? null, loading: false });
            }
        }
    }
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                    <Text style={{ flex: .5, fontSize: 16 }}>Loan Amount :</Text>
                                    <Text style={{ flex: .5, fontSize: 16, }}>{this.state.loan.paid_date} ₹</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                    <Text style={{ flex: .5, fontSize: 16 }}>SLABS :</Text>
                                    <Text style={{ flex: .5, fontSize: 16, }}>{this.state.loan.slabs}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                    <Text style={{ flex: .5, fontSize: 16 }}>LOAN DATE :</Text>
                                    <Text style={{ flex: .5, fontSize: 16, }}>{this.state.loan.loan_amount}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                    <Text style={{ flex: .5, fontSize: 16 }}>EMI AMOUNT :</Text>
                                    <Text style={{ flex: .5, fontSize: 16, }}>{this.state.loan.emi_amount} ₹</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                    <Text style={{ flex: .5, fontSize: 16 }}>DUE EMI :</Text>
                                    <Text style={{ flex: .5, fontSize: 16, }}>{this.state.loan.remaining_emi}</Text>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 5, marginBottom: 3 }}>
                                    <Text style={{ flex: .5, fontSize: 16 }}>Transaction Details :</Text>
                                </View>
                                <View>
                                    {
                                        this.state.loan.transaction_details ?
                                            <View style={style.Row}>
                                                <Text style={{ flex: 3, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>Date</Text>
                                                <Text style={{ flex: 4, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1 }}>Payment Mode</Text>
                                                <Text style={{ flex: 3, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>Amount</Text>
                                            </View>
                                            : ''
                                    }
                                </View>
                                <View>
                                    {
                                        this.state.loan.transaction_details && this.state.loan.transaction_details.map((row, i) => {
                                            return (
                                                <View style={style.Row}>
                                                    <Text style={{ flex: 3, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{row.date}</Text>
                                                    <Text style={{ flex: 4, padding: 10, borderColor: COLORS.primary, borderWidth: 1 }}>{row.payment_mode}</Text>
                                                    <Text style={{ flex: 3, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>{row.amount}</Text>
                                                </View>
                                            )
                                        })
                                    }
                                </View>
                            </View>
                    }
                </View>
            </ScrollView>
        );
    }
}