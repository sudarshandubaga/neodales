import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Picker, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
// import { Icon } from "react-native-vector-icons/Icon";
import Icon from 'react-native-vector-icons/dist/Ionicons';
import { calender } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Menu/style";

export default class LoanScreen extends Component {
    state = {
        loan: null,
        loading: true,
        status: 'All',
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `employee_loan/${user.Data.employee_id}`;

            let response = await calender(url);

            if (response.status) {
                this.setState({ loan: response.data.response ?? null, loading: false });
            }
        }
    }
    render() {
        let { navigation } = this.props;
        if (this.state.loan)
            console.log('response: ', this.state.loan);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View>
                                <View style={{ borderWidth: 2, borderColor: COLORS.primary, marginBottom: 15, borderRadius: 8 }}>
                                    <Picker
                                        selectedValue={this.state.status}
                                        style={{ height: 50, padding: 0, borderWidth: 1, borderColor: COLORS.primary }}
                                        onValueChange={status => this.setState({ status })}
                                    >
                                        <Picker.Item label="All" value="All" />
                                        <Picker.Item label="Active" value="Active" />
                                        <Picker.Item label="Closed" value="Closed" />
                                    </Picker>
                                </View>
                                {
                                    this.state.status == 'All' ?
                                        <>
                                            {
                                                this.state.loan && this.state.loan.data && this.state.loan.data.length ?
                                                    <>
                                                        {
                                                            this.state.loan && this.state.loan.data && this.state.loan.data.map((row, i) => {
                                                                return (
                                                                    <TouchableOpacity style={style.Row} onPress={() => navigation.push('LoanDetails', row)}>
                                                                        <View key={i} style={style.Column}>
                                                                            <View>
                                                                                <Text style={{ color: COLORS.primary, fontSize: 16 }}>
                                                                                    {row.paid_date}
                                                                                </Text>
                                                                                {/* <Text>{row.loan_amount}</Text> */}
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>Loan Amount :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.loan_amount} ₹</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>SLABS :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.slabs}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>EMI AMOUNT :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.emi_amount} ₹</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>DUE EMI :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.remaining_emi}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>STATUS :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.status}</Text>
                                                                                </View>
                                                                            </View>
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                )
                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <View>
                                                        <Text style={{ textAlign: 'center' }}>No record found.</Text>
                                                    </View>
                                            }
                                        </>
                                        :
                                        <>
                                            {
                                                this.state.loan && this.state.loan.data && this.state.loan.data.filter(item => item.status.includes(this.state.status)).length ?
                                                    <>
                                                        {
                                                            this.state.loan && this.state.loan.data && this.state.loan.data.filter(item => item.status.includes(this.state.status)).map((row, i) => {
                                                                return (
                                                                    <TouchableOpacity style={style.Row} onPress={() => navigation.push('LoanDetails', row)}>
                                                                        <View key={i} style={style.Column}>
                                                                            <View>
                                                                                <Text style={{ color: COLORS.primary, fontSize: 16 }}>{row.paid_date}</Text>
                                                                                {/* <Text>{row.loan_amount}</Text> */}
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>Loan Amount :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.loan_amount}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>SLABS :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.slabs}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>EMI AMOUNT :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.emi_amount}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>DUE EMI :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.remaining_emi}</Text>
                                                                                </View>
                                                                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                                                    <Text style={{ flex: .5, fontSize: 16 }}>STATUS :</Text>
                                                                                    <Text style={{ flex: .5, fontSize: 16, }}>{row.status}</Text>
                                                                                </View>
                                                                            </View>
                                                                        </View>
                                                                    </TouchableOpacity>
                                                                )
                                                            })
                                                        }
                                                    </>
                                                    :
                                                    <View>
                                                        <Text style={{ textAlign: 'center' }}>No record found.</Text>
                                                    </View>
                                            }
                                        </>
                                }
                            </View>

                    }
                </View>
            </ScrollView >
        );
    }
}