import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Button, Linking, ScrollView, StyleSheet, Text, View } from "react-native";
import { imprest } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Attendance/style";

export default class ImprestScreen extends Component {
    state = {
        imprest: null,
        loading: true
    }
    componentDidMount = async () => {
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
            let url = `imprest/${user.Data.student_id}/${user.Data.section_id}`;
            let response = await imprest(url);

            if (response.status) {
                this.setState({ imprest: response.response_code ?? null, loading: false });
            }
        }
    }
    render() {
        if (this.state.imprest)
            console.log('response: ', this.state.imprest);
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <>
                                <View style={{ marginBottom: 20, flexDirection: 'row', backgroundColor: COLORS.primary, padding: 10 }}>
                                    <Text style={{ flex: 5, color: COLORS.white }}>IMPREST BALANCE :</Text>
                                    <Text style={{ flex: 5, color: COLORS.white, textAlign: 'right' }}>373.00 ₹</Text>
                                </View>
                                <View>
                                    <View style={style.Row}>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>DATE</Text>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>BILL NO.</Text>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1 }}>CHARGED</Text>
                                        <Text style={{ flex: 2.5, padding: 10, textTransform: 'uppercase', borderColor: COLORS.primary, color: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>RECEIVED</Text>
                                    </View>
                                </View>
                                <View>
                                    {
                                        this.state.imprest && this.state.imprest.data ?
                                            <>
                                                {
                                                    this.state.imprest && this.state.imprest.data && this.state.imprest.data.map((row, i) => {
                                                        return (
                                                            <View style={style.Row}>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{row.date}</Text>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderRightWidth: 0 }}>{row.bill_no}</Text>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1 }}>{row.charged} ₹</Text>
                                                                <Text style={{ flex: 2.5, padding: 10, borderColor: COLORS.primary, borderWidth: 1, borderLeftWidth: 0 }}>{row.received} ₹</Text>
                                                            </View>
                                                        )
                                                    })
                                                }
                                            </>
                                            :
                                            <View style={style.Row}>
                                                <Text style={{ flex: 10, padding: 10, borderColor: COLORS.primary, borderWidth: 1, textAlign: 'center' }}>No Record Found.</Text>
                                            </View>
                                    }
                                </View>
                            </>

                    }
                </View>
            </ScrollView >
        );
    }
}