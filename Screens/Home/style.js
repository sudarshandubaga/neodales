import { StyleSheet } from "react-native";
import { SCREEN } from "../../configs/constants.config";

export default StyleSheet.create({
    container: {
        padding: 5
    },
    Row: {
        flexDirection: 'row',
        // justifyContent: 'center',
        flexWrap: 'wrap'
    },
    Column: {
        width: SCREEN.width / 3 - 10 / 3,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#ebebeb',
        borderBottomWidth: 1,
        borderRightWidth: 1,
        paddingVertical: 15
    },
    noBorder: {
        borderRightWidth: 0
    },
    homeIcons: {
        width: 40,
        height: 40,
        marginBottom: 15,
        resizeMode: 'contain'
    },
    homeIconText: {
        textAlign: 'center',
        fontSize: 10
    }
});