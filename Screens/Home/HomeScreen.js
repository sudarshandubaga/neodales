import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { Image, Linking, RefreshControl, ScrollView, Text, TouchableOpacity, View } from "react-native";
import style from "./style";

let wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

export default class HomeScreen extends Component {
    state = {
        user: null,
        menus: null,
        s_icons: [
            // {
            //     icon: require('../../Assets/img1.png'),
            //     text: 'MENU OF THE MONTH',
            //     path: 'Menu',
            //     is_url: false,
            // },
            // {
            //     icon: require('../../Assets/img2.png'),
            //     text: 'AMAZING FRIDAY',
            //     path: 'AmazingFriday',
            //     is_url: false,
            // },
            {
                icon: require('../../Assets/student/personal-info.png'),
                text: 'PERSONAL INFO',
                path: 'Profile',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/attendance.png'),
                text: 'ATTENDANCE INFO',
                path: 'Attendance',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/siblings.png'),
                text: 'SIBLING',
                path: 'Siblings',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/diary.png'),
                text: 'E-DIARY',
                path: 'Ediary',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/assignment.png'),
                text: 'ASSIGNMENTS',
                path: 'Assignments',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/video.png'),
                text: 'VIDEO LECTURES',
                path: 'VideosLectures',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/fees.png'),
                text: 'FEES INFO',
                path: 'Fees',
                is_url: false,
            },
            {
                icon: require('../../Assets/img7.png'),
                text: 'ONLINE FEE DEPOSIT',
                path: 'https://www.google.com', // 'https://www.eduqfix.com/PayDirect/#/student/pay/J4j5I4LjzIaBTjwnOUkPCagEwqE4Uy6gax6lS5sQPEGkBCn9kBUIIQT3V1tCGui2/766',
                is_url: true,
            },
            {
                icon: require('../../Assets/student/transport.png'),
                text: 'TRANSPORT INFO & FEE',
                path: 'Transport',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/timetable.png'),
                text: 'TIME TABLE',
                path: 'Timetable',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/test.png'),
                text: 'CLASS TEST',
                path: 'ClassTest',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/calender.png'),
                text: 'CALENDAR',
                path: 'Calender',
                is_url: false,
            },
            {
                icon: require('../../Assets/img3.png'),
                text: 'HOLIDAY OF THE MONTH',
                path: 'Holiday',
                is_url: false,
            },
            {
                icon: require('../../Assets/img4.png'),
                text: 'FORTHCOMING EVENTS',
                path: 'Event',
                is_url: false,
            },
            {
                icon: require('../../Assets/img8.png'),
                text: 'NEWS',
                path: 'News',
                is_url: false,
            },
            {
                icon: require('../../Assets/img5.png'),
                text: 'MESMERISING MOMENTS',
                path: 'Gallery',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/video.png'),
                text: 'VIDEO GALLERY',
                path: 'Videos',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/leave.png'),
                text: 'APPLY LEAVE',
                path: 'Leave',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/download.png'),
                text: 'DOWNLOADS',
                path: 'Download',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/question.png'),
                text: 'ASK QUESTION',
                path: 'Question',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/contact.png'),
                text: 'CONTACT US',
                path: 'Contact',
                is_url: false,
            },
            // {
            //     icon: require('../../Assets/student/imprest.png'),
            //     text: 'IMPREST INFO',
            //     path: 'Imprest',
            //     is_url: false,
            // },
        ],
        e_icons: [
            // {
            //     icon: require('../../Assets/img1.png'),
            //     text: 'MENU OF THE MONTH',
            //     path: 'Menu',
            //     is_url: false,
            // },
            // {
            //     icon: require('../../Assets/img2.png'),
            //     text: 'AMAZING FRIDAY',
            //     path: 'AmazingFriday',
            //     is_url: false,
            // },
            {
                icon: require('../../Assets/img3.png'),
                text: 'HOLIDAY OF THE MONTH',
                path: 'Holiday',
                is_url: false,
            },
            {
                icon: require('../../Assets/img4.png'),
                text: 'FORTHCOMING EVENTS',
                path: 'Event',
                is_url: false,
            },
            {
                icon: require('../../Assets/img5.png'),
                text: 'MESMERISING MOMENTS',
                path: 'Gallery',
                is_url: false,
            },
            {
                icon: require('../../Assets/img7.png'),
                text: 'ONLINE FEE DEPOSIT',
                path: 'https://www.google.com', // 'https://www.eduqfix.com/PayDirect/#/student/pay/J4j5I4LjzIaBTjwnOUkPCagEwqE4Uy6gax6lS5sQPEGkBCn9kBUIIQT3V1tCGui2/766',
                is_url: true,
            },
            {
                icon: require('../../Assets/img8.png'),
                text: 'NEWS',
                path: 'News',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/personal-info.png'),
                text: 'PERSONAL INFO',
                path: 'Profile',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/attendance.png'),
                text: 'ATTENDANCE INFO',
                path: 'AttendanceStaff',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/salary.png'),
                text: 'SALARY DETAILS',
                path: 'Salary',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/service.png'),
                text: 'SERVICE RECORD',
                path: 'Service',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/complant.png'),
                text: 'COMPLANT STATUS',
                path: 'Complant',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/calender.png'),
                text: 'CALENDER',
                path: 'Calender',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/leave.png'),
                text: 'APPLY LEVAVE',
                path: 'Leave',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/leave-status.png'),
                text: 'LEAVE STATUS',
                path: 'LeaveStatus',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/timetable.png'),
                text: 'TIME TABLE',
                path: 'Timetable',
                is_url: false,
            },
            {
                icon: require('../../Assets/student/loan.png'),
                text: 'LOAN DETAILS',
                path: 'Loan',
                is_url: false,
            }
        ]
    }

    async componentDidMount() {
        let self = this;
        let integration = await AsyncStorage.getItem('@integration');
        integration = JSON.parse(integration);
        console.log('integration: ', integration);

        let state = this.state;

        let s_icons = state.s_icons;
        let e_icons = state.e_icons;

        s_icons[7].path = integration.fees_url;
        e_icons[3].path = integration.fees_url;

        console.log('integration: ', integration);

        // setTimeout(() => {
        //     self.setState(
        //         s_icons,
        //         e_icons
        //     );
        // }, 1);


        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
            if (user && user.UserType == "S") {
                this.setState({ menus: this.state.s_icons });
                // menus = this.state.s_icons;
            } else {
                this.setState({ menus: this.state.e_icons });
                // menus = this.state.e_icons;
            }
        } else {
            this.goTo('Login');
        }
    }

    navigateTo = (path, params = null) => {
        let { navigation } = this.props;

        navigation.push(path, params);
    }
    goTo = path => {
        let { navigation } = this.props;
        navigation.push(path);
    }

    onRefresh = async () => {

        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            console.log('user details', user);
            this.setState({ user: user });
            if (user && user.UserType == "S") {
                this.setState({ menus: this.state.s_icons });
                // menus = this.state.s_icons;
            } else {
                this.setState({ menus: this.state.e_icons });
                // menus = this.state.e_icons;
            }
        } else {
            this.goTo('Login');
        }

        wait(2000).then(() => this.setState({ refreshing: false }));
    }
    render() {
        // console.log('menus', this.state.menus);
        return (
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={() => this.onRefresh()}
                    />
                }
            >
                <View style={style.container}>
                    <View style={style.Row}>
                        {
                            this.state.menus && this.state.menus.map((row, i) => {
                                var classArr = [style.Column];
                                if ((i + 1) % 3 === 0) {
                                    classArr.push(style.noBorder);
                                }
                                return (
                                    <TouchableOpacity style={classArr} key={i} onPress={() => row.is_url ? Linking.openURL(row.path) : this.navigateTo(row.path)}>
                                        <Image
                                            source={row.icon}
                                            resizeMode="contain"
                                            style={style.homeIcons}
                                        />
                                        <Text style={style.homeIconText}>{row.text}</Text>
                                    </TouchableOpacity>
                                );
                            })
                        }
                    </View>
                </View>
            </ScrollView>
        );
    }
}