import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { Component } from "react";
import { ActivityIndicator, Alert, ColorPropType, Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import { applyleave } from "../../Api/student";
import { COLORS } from "../../configs/constants.config";
import style from "../Leave/style";

export default class ChangePasswordScreen extends Component {
    state = {
        user: null,
        menus: null,
        loading: true,
        new_password: null,
        confirm_password: null,
        old_password: null,
    }
    componentDidMount = async () => {
        this.setState({ loading: false });
        let user = await AsyncStorage.getItem('@user');
        if (user) {
            user = JSON.parse(user);
            this.setState({ user: user });
        }
    }
    changePassword = async () => {
        let user = this.state.user;

        let {
            new_password,
            confirm_password,
            old_password
        } = this.state;
        if (old_password == null) {
            Alert.alert(
                "Error",
                'Please enter old password.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else if (new_password == null) {
            Alert.alert(
                "Error",
                'Please enter new password.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else if (confirm_password == null) {
            Alert.alert(
                "Error",
                'Please enter confirm password.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else if (new_password != confirm_password) {
            this.setState({ msg: 'Please enter same new password & confirm password.' });
            Alert.alert(
                "Error",
                'Please enter same new password & confirm password.',
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ]
            );
        } else {
            let url
            if (user.Data.UserType == 'S') {
                url = `change_password/${user.Data.enrollment_number}/${this.state.old_password}/${this.state.new_password}`;
            } else {
                url = `change_password/${user.EmployeeData.User.username}/${this.state.old_password}/${this.state.new_password}`;
            }
            let response = await applyleave(url);
            console.log('url', response);
            if (response.status) {
                Alert.alert(
                    "Alert",
                    response.data.response_message,
                    [
                        { text: "OK", onPress: () => console.log("OK Pressed") }
                    ]
                );
                if (response.data.response_code) {
                    this.setState({ confirm_password: null });
                    this.setState({ old_password: null });
                    this.setState({ new_password: null });
                }
            }
        }
    }
    render() {
        return (
            <ScrollView>
                <View style={style.container}>
                    {
                        this.state.loading
                            ?
                            <View style={{ padding: 10 }}>
                                <ActivityIndicator size="large" color={COLORS.primary} />
                            </View>
                            :
                            <View style={{}}>
                                <View style={{ position: 'absolute', top: 40, left: 120, borderRadius: 100, borderWidth: 1, borderColor: COLORS.primary, backgroundColor: COLORS.white, zIndex: 1 }}>
                                    <View style={{ height: 110, width: 110 }}>
                                    </View>
                                </View>
                                <View style={{ borderWidth: 1, padding: 20, marginTop: 100, borderColor: COLORS.primary, borderRadius: 8 }}>
                                    <View style={{ paddingTop: 40 }}>
                                        <View style={style.formGroup}>
                                            <TextInput name="password" secureTextEntry={true} style={style.textInput} placeholder="Old Password" value={this.state.old_password} onChangeText={old_password => this.setState({ old_password })} placeholderTextColor={COLORS.primary} />
                                        </View>
                                        <View style={style.formGroup}>
                                            <TextInput name="password" secureTextEntry={true} style={style.textInput} placeholder="New Password" value={this.state.new_password} onChangeText={new_password => this.setState({ new_password })} placeholderTextColor={COLORS.primary} />
                                        </View>
                                        <View style={style.formGroup}>
                                            <TextInput name="password" secureTextEntry={true} style={style.textInput} placeholder="Confirm Password" value={this.state.confirm_password} onChangeText={confirm_password => this.setState({ confirm_password })} placeholderTextColor={COLORS.primary} />
                                        </View>
                                        <TouchableOpacity style={style.btnPrimary} onPress={() => this.changePassword()}>
                                            <Text style={{ color: COLORS.white, textAlign: 'center' }}>Change Password</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                    }
                </View>
            </ScrollView >
        );
    }
}